// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "statuswidget.h"

#include <QHBoxLayout>
#include <QFontDatabase>

StatusWidget::StatusWidget(const QString &text, const QString &icon, int status, int count, QWidget *parent)
    : QWidget(parent), status(status), text(text), icon(icon)
{
    this->labelIcon = new QLabel();
    this->labelText = new QLabel();
    this->labelCount = new QLabel();

    this->labelIcon->setText(icon);

    this->setStatus(status);
    this->labelCount->setNum(count);

    this->setCount(count);

    // Load FontAwesome (if possible)
    this->fa = nullptr;
    if (QFontDatabase::addApplicationFont(":/fonts/fonts/Font Awesome 5 Free-Solid-900.otf") >= 0) {
        this->fa = new QFont("Font Awesome 5 Free", 8, 0, false);
        this->fa->setStyleName("Solid");
        this->labelIcon->setFont(*this->fa);
    }

    QHBoxLayout *ly = new QHBoxLayout;
    ly->setSpacing(4);
    ly->addWidget(this->labelIcon);
    ly->addWidget(this->labelText);
    ly->addWidget(this->labelCount);
    this->setLayout(ly);
}

int
StatusWidget::getStatus()
{
    return this->status;
}

void
StatusWidget::setCount(int count)
{
    this->labelCount->setNum(count);
    if (count > -1) {
        this->labelText->setText(this->text + ":");
        this->labelCount->setVisible(true);
    } else {
        this->labelText->setText(this->text);
        this->labelCount->setVisible(false);
    }
}

void
StatusWidget::setStatus(int status)
{
    this->labelIcon->setVisible(true);
    if (this->fa != nullptr) {
        switch (status) {
        case status::none:
            if (this->icon == "") {
                this->labelIcon->setVisible(false);
            } else {
                this->labelIcon->setText(this->icon);
            }
            break;
        case status::ok:
            this->labelIcon->setText("\uf14a");
            break;
        case status::empty:
            this->labelIcon->setText("\uf0c8");
            break;
        case status::error:
            this->labelIcon->setText("\uf071");
            break;
        case status::changed:
            this->labelIcon->setText("\uf085");
            break;
        default:
            this->labelIcon->setText("\uf059");
            break;
        }
    } else {
        switch (status) {
        case status::none:
            this->labelIcon->setText("");
            this->labelIcon->setVisible(false);
            break;
        case status::ok:
            this->labelIcon->setText("✔");
            break;
        case status::empty:
            this->labelIcon->setText("☐");
            break;
        case status::error:
            this->labelIcon->setText("⚠");
            break;
        case status::changed:
            this->labelIcon->setText("⚙");
            break;
        default:
            this->labelIcon->setText("<b>?</b>");
            break;
        }
    }
}
