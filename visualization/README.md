# MIAMIapp visualization folder

MIAMIapp visualization depends on two external sources:

-   [MIAMIviz](https://gitlab.com/miamisuite/miamiviz)
-   [D3.js](https://d3js.org)

## MIAMIviz

Copy following files from the [MIAMIviz repository](https://gitlab.com/miamisuite/miamiviz) to this folder:

-   `index.html`
-   `miamiviz.css`
-   `bboxCollide.js`
-   `miamiviz.js`

## D3.js

Copy the following file from [D3.js (version 5)](https://github.com/d3/d3/releases/tag/v5.16.0) to this folder:

-   `d3.js.min` (renamed to `d3.v5.min.js`)

This is the latest version of D3.js tested.
