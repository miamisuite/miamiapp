// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef STATUSWIDGET_H
#define STATUSWIDGET_H

#include <QWidget>
#include <QLabel>


enum status {
    none = -1,
    empty = 0,
    ok = 1,
    changed = 2,
    error = 3
};

class StatusWidget : public QWidget
{
    Q_OBJECT
public:
    explicit StatusWidget(const QString &labelText, const QString &icon = "", const int status = -1, int labelCount = -1, QWidget *parent = nullptr);
    int getStatus();
    void setStatus(int status);
    void setCount(int count);

private:
    int status;
    QString text;
    QString icon;
    QLabel *labelIcon;
    QLabel *labelText;
    QLabel *labelCount;
    QFont *fa;

signals:

public slots:
};

#endif // STATUSWIDGET_H
