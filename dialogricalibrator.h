// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef DIALOGRICALIBRATOR_H
#define DIALOGRICALIBRATOR_H

#include <ricalculator.h>
#include <librarysearch.h>
#include <gcmsdiskscan.h>

#include "qcustomplot.h"

#include <QDialog>
#include <QSettings>
#include <QtConcurrent>

namespace Ui {
class DialogRiCalibrator;
}

class DialogRiCalibrator : public QDialog
{
    Q_OBJECT

public:
    explicit DialogRiCalibrator(QWidget *parent = nullptr);
    ~DialogRiCalibrator();
    gcms::LibrarySearch<int, float> *lib;
    gcms::RICalculator riCalc;
    gcms::GCMSDiskScan<int, float> *chromatogram;

private:
    Ui::DialogRiCalibrator *ui;

    void openLibrary(const QString &file);
    void calibrateFiles();
    void replotTic();
    void replotSpectrum();

    int cntAll;
    int cntOk;

    QCustomPlot *plotSpectrum;

    QSettings *settings;

    QFutureWatcher<void> *thread;
    std::vector<gcms::Compound<int, float>*> compounds;
    QMap<QString, QList<QList<double>>> mappingAll;
    QMap<QString, int> mappingSelected;
    QStringList calibrate_files;
};

#endif // DIALOGRICALIBRATOR_H
