# MIAMIapp

Full Qt5 application for Mode of Action analysis

## Development environment

MIAMIapp is build with [Qt5](https://www.qt.io/)

## C++ dependencies

To compile MIAMI app you need the following libraries:

-   libgcms from [MetaboliteDetector](https://md.tu-bs.de) (precompiled)
-   [JSON for Modern C++](https://github.com/nlohmann/json) (see [README](nlohmann/README.md))

## Additional dependencies

MIAMIapp also needs Fontawesome 5 Free (see [README](fonts/README.md)) and MIAMIviz (see [README](visualization/README.md)).
