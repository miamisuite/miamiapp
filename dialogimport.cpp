// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "dialogimport.h"
#include "ui_dialogimport.h"

#include "gzstream.h"

#include <QtConcurrent>

#include <gcmsanalyzer.h>
#include <defaultpeakdetector.h>
#include <cdfimporter.h>
#include <compound.h>


DialogImport::DialogImport(QWidget *parent, QStringList files) :
    QDialog(parent),
    ui(new Ui::DialogImport)
{
    ui->setupUi(this);

    ui->buttonDelete->setEnabled(false);
    ui->buttonImport->setEnabled(false);

    ui->progressBar->setRange(0, 0);
    ui->progressBar->setVisible(false);

    ui->labelStatus->setVisible(false);


    modelFiles = new QStringListModel();
    modelFiles->setStringList(files);
    ui->listFiles->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listFiles->setModel(modelFiles);

    importFuture = new QFutureWatcher<QList<QString>>();
    deconvolutionFuture = new QFutureWatcher<void>();

    connect(this->modelFiles, &QStringListModel::rowsRemoved, [=]() {
            ui->buttonDelete->setEnabled(this->modelFiles->rowCount() > 0);
            ui->buttonImport->setEnabled(this->modelFiles->rowCount() > 0);
    });

    connect(ui->listFiles, &QListView::clicked, [=](){
        ui->buttonDelete->setEnabled(true);
    });

    connect(ui->buttonAdd, &QPushButton::clicked, [=]() {
        QSettings settings("TU Braunschweig", "MiamiViz");
        QVariant last_cdf = settings.value("last_cdf", QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));

        QFileDialog dialog(this, tr("Select netCdf files to import or .bin files for deconvolution"));
        dialog.setFileMode(QFileDialog::ExistingFiles);
        dialog.setDirectory(last_cdf.toString());
        dialog.setNameFilter("netCdf files (*.cdf);;MD files (*.bin)");

        if (dialog.exec()) {
            QStringList files = dialog.selectedFiles();
            for (auto file : files) {
                int row = modelFiles->rowCount();
                modelFiles->insertRow(row);
                modelFiles->setData(modelFiles->index(row), file);
            }
            settings.setValue("last_cdf", QFileInfo(files[0]).path());
            settings.sync();

            ui->buttonImport->setEnabled(this->modelFiles->rowCount() > 0);
        }
    });

    importFuture = new QFutureWatcher<QList<QString>>(this);
    connect(importFuture, &QFutureWatcher<QList<QString>>::finished, [=]() {
        // Start deconvolution when finished
        this->deconvoluteFiles(importFuture->result());
    });

    deconvolutionFuture = new QFutureWatcher<void>(this);
    connect(deconvolutionFuture, &QFutureWatcher<void>::finished, [=]() {
            ui->labelStatus->setText("Done!");
            ui->buttonAdd->setEnabled(true);
            ui->buttonDelete->setEnabled(true);
            ui->buttonClose->setEnabled(true);
            ui->progressBar->setVisible(false);
            ui->checkCloseWhenDone->setEnabled(true);

            if (ui->checkCloseWhenDone->isChecked()) {
                this->close();
            }
    });

    connect(ui->buttonImport, &QPushButton::clicked, [=]() {
        ui->buttonAdd->setEnabled(false);
        ui->buttonDelete->setEnabled(false);
        ui->buttonClose->setEnabled(false);
        ui->buttonImport->setEnabled(false);
        ui->checkCloseWhenDone->setEnabled(false);
        ui->progressBar->setVisible(true);
        ui->labelStatus->setVisible(true);

        importFuture->setFuture(QtConcurrent::run([=]() {
            return this->importFiles(this->modelFiles->stringList());
        }));

    });

    connect(ui->buttonDelete, &QPushButton::clicked, [=]() {
        modelFiles->removeRows(ui->listFiles->currentIndex().row(), 1);
    });

    connect(ui->buttonClose, &QPushButton::clicked, [=]() {
        this->close();
    });
}

QList<QString>
DialogImport::importFiles(const QList<QString> &files) {
    ui->labelStatus->setText("Importing files...");
    if (files.empty()) {
        return QStringList();
    }

    QList<QString> res;
    for (auto file : files) {
        QString r = DialogImport::importFile(file);
        if(r != "") {
            res.append(r);
        }
    }
    return res;
}


QString
DialogImport::importFile(const QString &file) {

    QString res = file;

    QFileInfo f(file);
    QString ext = f.suffix();
    if (ext == "bin") {
        res = res.replace(".bin", "");
    } else {
        res = res.replace(".cdf", "");    // Import the file
        try {
            gcms::CdfImporter cdf(file.toStdString().c_str());
            cdf.importData(res.toStdString().c_str());
        } catch (gcms::CdfImporterException e) {
            return "";
        }
    }
    return res;
}

void
DialogImport::deconvoluteFiles(const QList<QString> &files) {
    ui->labelStatus->setText("Deconvolution...");
    gcms::GCMSSettings::AN_PEAK_THRESHOLD_BEGIN = ui->spinSensitivity->value();
    gcms::GCMSSettings::AN_PEAK_THRESHOLD_END = ui->spinSensitivity->value()*-1.0;
    gcms::GCMSSettings::AN_MIN_PEAK_HEIGHT = ui->spinSensitivity->value();


//    gcms::GCMSSettings::AN_PEAK_THRESHOLD_BEGIN = ui->spinPeakThreshold->value();
//    gcms::GCMSSettings::AN_PEAK_THRESHOLD_END = ui->spinPeakThreshold->value()*-1.0;
//    gcms::GCMSSettings::AN_MIN_PEAK_HEIGHT = ui->spinMinPeakHeight->value();
    gcms::GCMSSettings::AN_NUMBER_BINS = 10;
    gcms::GCMSSettings::AN_DECONVOLUTION_WIDTH = ui->spinDeconvolutionWidth->value();
    deconvolutionFuture->setFuture(QtConcurrent::map(files, DeconvolutionWrapper()));
}

void
DialogImport::deconvoluteFile(const QString &file) {
    // Deconvolute the file
    gcms::GCMSDiskScan<int, float> chromatogram(file.toStdString().c_str());
    gcms::DefaultPeakDetector<int, float> *detector = new gcms::DefaultPeakDetector<int, float>();
    gcms::GCMSAnalyzer<int, float> analyzer(chromatogram, detector);

    chromatogram.setBaselineCorrectionEnabled(false);

    analyzer.detectAllPeaks(false);
    analyzer.deconvoluteChromatogram();

    std::vector<gcms::Compound<int,float>*> compounds = analyzer.getCompounds();
    const gcms::RICalculator &ric = chromatogram.getRICalculator();
    ric.calculateRI ( compounds );

    QString outFile = file + ".cmp";
    ogzstream outStream(outFile.toStdString().c_str());
    // Save compounds in .cmp file
    gcms::Compound<int, float>::compoundsToStream(compounds, outStream);
    // Save gcms settings in .cmp file
    gcms::GCMSSettings::toStream(outStream);

    // Cleanup
    outStream.close();
    for (auto cmp : compounds) {
        delete cmp;
    }
//    delete detector;
}

DialogImport::~DialogImport()
{
    delete ui;
}
