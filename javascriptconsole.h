// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef JAVASCRIPTCONSOLE_H
#define JAVASCRIPTCONSOLE_H

#include <QWidget>

class MiamiWebEnginePage;

namespace Ui {

class JavascriptConsole;
}

class JavascriptConsole : public QWidget
{
    Q_OBJECT

public:
    explicit JavascriptConsole(QWidget *parent = nullptr);
    ~JavascriptConsole();
    void setPage(MiamiWebEnginePage *page);
    void log(const QString &msg, int line, const QString &file);
    QStringList history;

private:
    Ui::JavascriptConsole *ui;

    MiamiWebEnginePage *page;
};

#endif // JAVASCRIPTCONSOLE_H
