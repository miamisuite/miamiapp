// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "miamiwebenginepage.h"

MiamiWebEnginePage::MiamiWebEnginePage()
{
}

QWebEnginePage* MiamiWebEnginePage::createWindow(QWebEnginePage::WebWindowType type)
{
    qDebug() << type;
    return new MiamiWebEnginePage();
}



bool MiamiWebEnginePage::acceptNavigationRequest(const QUrl & url, QWebEnginePage::NavigationType type, bool isMainFrame)
{
    if (isMainFrame && type == QWebEnginePage::NavigationTypeLinkClicked) {
        qDebug() << "acceptNavigationRequest";
        QDesktopServices::openUrl(url);
        delete this;
        return false;
    } else {
        qDebug() << type;
    }
    return true;
}

void
MiamiWebEnginePage::registerConsole(JavascriptConsole *console) {
    this->console = console;
}

void MiamiWebEnginePage::javaScriptConsoleMessage(QWebEnginePage::JavaScriptConsoleMessageLevel level,
                                                  const QString &message, int lineNumber, const QString &file)
{
    if (this->console) {
        this->console->log(message, lineNumber, file);
    } else {
        if (level != QWebEnginePage::JavaScriptConsoleMessageLevel::ErrorMessageLevel) {
            qDebug() << QString("[%1:%2] Error: %3").arg(file).arg(lineNumber).arg(message);
        } else if(level == QWebEnginePage::JavaScriptConsoleMessageLevel::WarningMessageLevel) {
            qDebug() << QString("[%1:%2] Warning: %3").arg(file).arg(lineNumber).arg(message);
        } else if (level==QWebEnginePage::JavaScriptConsoleMessageLevel::InfoMessageLevel) {
            qDebug() << message;
        }
    }
}
