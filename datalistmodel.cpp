// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <QtWidgets>

#include "datalistmodel.h"

DataListModel::DataListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int DataListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return lstNames.count();
}

QVariant DataListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    switch(role) {
    case Qt::EditRole:
    case Qt::DisplayRole: {
        return lstNames.at(index.row());
    }
    case Qt::UserRole: {
        return lstData.at(index.row());
    }
    default: {
        return QVariant();
    }
    }
}

void DataListModel::setType(int idx) {
    this->idx = idx;
}

bool DataListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.row() >= 0 && index.row() < lstNames.size()) {
        switch(role) {
        case Qt::DisplayRole:
        case Qt::EditRole: {
            const QString valueString = value.toString();
            if (lstNames.at(index.row()) == valueString)
                return true;
            lstNames.replace(index.row(), valueString);
            emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});
            return true;
        }
        case Qt::UserRole: {
            if (lstData.at(index.row()) == value)
                return true;
            lstData.replace(index.row(), value);
            emit dataChanged(index, index, {Qt::UserRole});
            return true;
        }
        default: {
            return false;
        }
        }
    }
    return false;
}

bool DataListModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (count <= 0 || row < 0 || (row + count) > rowCount(parent))
        return false;
    beginRemoveRows(QModelIndex(), row, row + count - 1);
    const auto it = lstNames.begin() + row;
    lstNames.erase(it, it + count);
    const auto it2 = lstData.begin() + row;
    lstData.erase(it2, it2 + count);

    endRemoveRows();
    return true;
}

Qt::ItemFlags DataListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return QAbstractListModel::flags(index) | Qt::ItemIsDropEnabled;
    return QAbstractListModel::flags(index) | Qt::ItemIsEditable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
}

bool DataListModel::insertRows(int row, int count, const QModelIndex &parent)
{
    if (count < 1 || row < 0 || row > rowCount(parent))
        return false;
    beginInsertRows(QModelIndex(), row, row + count - 1);
    for (int r = 0; r < count; ++r){
        lstNames.insert(row, QString());
        lstData.insert(row, QVariant());
    }
    endInsertRows();
    return true;
}

static bool decendingNames(const QPair<QString, int> &s1, const QPair<QString, int> &s2)
{
    if (s1.first.contains(QRegExp("^RI [0-9\\.]*"))) {
         return false;
    }
    return s1.first > s2.first;
}

void DataListModel::sort(int, Qt::SortOrder order)
{
    emit layoutAboutToBeChanged(QList<QPersistentModelIndex>(), VerticalSortHint);
    QVector<QPair<QString, int> > listNames;
    QVector<QPair<QVariant, int> > listData;
    const int lstCount = lstNames.count();
    listNames.reserve(lstCount);
    listData.reserve(lstCount);
    for (int i = 0; i < lstCount; ++i){
        listNames.append(QPair<QString, int>(lstNames.at(i), i));
        listData.append(QPair<QVariant, int>(lstData.at(i), i));
    }
    if (order == Qt::AscendingOrder) {
        std::sort(listNames.begin(), listNames.end(), [=](const QPair<QString, int> &s1, const QPair<QString, int> &s2) {
            if (this->idx == 2 && s1.first.contains(QRegExp("^RI [0-9\\.]+"))) {
                 return false;
            } else if (this->idx == 2 && s1.first.contains(QRegExp("^RI [0-9\\.]+"))) {
                return true;
            }
            QRegularExpression re("(RI|RT) ([\\d\\.]+)");
            auto m1 = re.match(s1.first);
            auto m2 = re.match(s2.first);
            if (m1.hasMatch() && m2.hasMatch()) {
                return m1.captured(2).toDouble() < m2.captured(2).toDouble();
            }

            return s1.first < s2.first;
        });
        std::sort(listData.begin(), listData.end(), [=](const QPair<QVariant, int> &s1, const QPair<QVariant, int> &s2) {
            if (this->idx == 2 && s1.first.toStringList()[2].contains(QRegExp("^RI [0-9\\.]+"))) {
                 return false;
            }
            if (this->idx == 2) {
                return s1.first.toStringList()[this->idx] < s2.first.toStringList()[this->idx];
            } else {
                return s1.first.toStringList()[this->idx].toDouble() < s2.first.toStringList()[this->idx].toDouble();
            }

        });
    } else{
        std::sort(listNames.begin(), listNames.end(), decendingNames);
        std::sort(listData.begin(), listData.end(), [=](const QPair<QVariant, int> &s1, const QPair<QVariant, int> &s2){
            if (this->idx == 2 && s1.first.toStringList()[2].contains(QRegExp("^RI [0-9\\.]+"))) {
                 return false;
            } else if ((this->idx == 2 && s2.first.toStringList()[2].contains(QRegExp("^RI [0-9\\.]+")))) {
                return true;
            }
            return s1.first.toStringList()[this->idx] > s2.first.toStringList()[this->idx];
        });
    }
    lstNames.clear();
    lstData.clear();
    QVector<int> forwarding(lstCount);
    for (int i = 0; i < lstCount; ++i) {
        lstNames.append(listNames.at(i).first);
        lstData.append(listData.at(i).first);
        forwarding[listNames.at(i).second] = i;
    }
    QModelIndexList oldList = persistentIndexList();
    QModelIndexList newList;
    const int numOldIndexes = oldList.count();
    newList.reserve(numOldIndexes);
    for (int i = 0; i < numOldIndexes; ++i)
        newList.append(index(forwarding.at(oldList.at(i).row()), 0));
    changePersistentIndexList(oldList, newList);
    emit layoutChanged(QList<QPersistentModelIndex>(), VerticalSortHint);
}
