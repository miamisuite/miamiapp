// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef MIAMIWEBENGINEPAGE_H
#define MIAMIWEBENGINEPAGE_H

#include "javascriptconsole.h"

#include <QtWebEngineWidgets>


class MiamiWebEnginePage : public QWebEnginePage
{
public:
    MiamiWebEnginePage();
    void registerConsole(JavascriptConsole *console);

private:
    bool acceptNavigationRequest(const QUrl & url, QWebEnginePage::NavigationType type, bool isMainFrame);
    void javaScriptConsoleMessage(QWebEnginePage::JavaScriptConsoleMessageLevel level,
                                  const QString &message, int lineNumber, const QString &file);

    JavascriptConsole *console;

    QWebEnginePage* createWindow(QWebEnginePage::WebWindowType type);
};

#endif // MIAMIWEBENGINEPAGE_H
