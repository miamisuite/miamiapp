// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "texthistoryedit.h"

#include<QtWidgets>

TextHistoryEdit::TextHistoryEdit(QWidget *parent) : QLineEdit(parent)
{
}

void TextHistoryEdit::keyPressEvent(QKeyEvent *e) {
    bool update = false;
    if (e->key() == Qt::Key_Up) {
        if(this->idx < this->history.size()-1){
            this->idx++;
            update = true;
        }
    } else if (e->key() == Qt::Key_Down) {
        if (this->idx > -1) {
            this->idx--;
            update = true;
        }
    } else if (e->key() == Qt::Key_Return) {
        this->history << this->text();
        this->idx = -1;
    }
    if (update) {
        if(this->idx > -1 && this->idx < this->history.size()) {
            this->setText(this->history[this->history.size()-1-this->idx]);
        } else {
            this->setText("");
        }
    }

    QLineEdit::keyPressEvent(e);
}
