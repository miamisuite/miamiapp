// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <QtWidgets>

#include "dialogaddcondition.h"
#include "ui_dialogaddcondition.h"

DialogAddCondition::DialogAddCondition(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAddCondition)
{
    ui->setupUi(this);
    ui->buttonDelLabeled->setEnabled(false);
    ui->buttonDelUnlabeled->setEnabled(false);


    connect(ui->lineEditName, &QLineEdit::editingFinished, [=]() {
        this->name = ui->lineEditName->text();
    });


    // List Models
    this->modelLabeled = new QStringListModel();
    this->modelLabeled->setStringList(QStringList());
    ui->listViewLabeled->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listViewLabeled->setModel(modelLabeled);

    connect(ui->listViewLabeled, &QListView::clicked, [=](){
        ui->buttonDelLabeled->setEnabled(true);
    });
    connect(this->modelLabeled, &QStringListModel::rowsRemoved, [=]() {
        if (this->modelLabeled->rowCount() == 0) {
            ui->buttonDelLabeled->setEnabled(false);
        }
    });

    connect(ui->buttonAddLabeled, &QPushButton::clicked, [=]() {
        QSettings *settings = new QSettings("TU Braunschweig", "MiamiViz");
        QVariant last_labeled = settings->value("last_labeled", QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));
        QFileDialog dialog(this, tr("Select labeled data files"));
        dialog.setFileMode(QFileDialog::ExistingFiles);
        dialog.setDirectory(last_labeled.toString());
        dialog.setNameFilter("MD compound files (*.cmp)");
        if (dialog.exec()) {
            QStringList files = dialog.selectedFiles();
            for (auto file : files) {
                this->addLabeled(file);
            }
            settings->setValue("last_labeled", QFileInfo(files[0]).path());
        }
    });
    connect(ui->buttonDelLabeled, &QPushButton::clicked, [=]() {
        QString file = ui->listViewLabeled->currentIndex().data().toString();
        auto itr = std::find(this->labeled_files.begin(), this->labeled_files.end(), file);
        this->labeled_files.erase(itr);
        modelLabeled->removeRows(ui->listViewLabeled->currentIndex().row(), 1);
    });


    this->modelUnlabeled = new QStringListModel();
    this->modelUnlabeled->setStringList(QStringList());
    ui->listViewUnlabeled->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listViewUnlabeled->setModel(modelUnlabeled);

    connect(ui->listViewUnlabeled, &QListView::clicked, [=](){
        ui->buttonDelUnlabeled->setEnabled(true);
    });
    connect(this->modelUnlabeled, &QStringListModel::rowsRemoved, [=]() {
        if (this->modelUnlabeled->rowCount() == 0) {
            ui->buttonDelUnlabeled->setEnabled(false);
        }
    });

    connect(ui->buttonAddUnlabeled, &QPushButton::clicked, [=]() {
        QSettings *settings = new QSettings("TU Braunschweig", "MiamiViz");
        QVariant last_unlabeled = settings->value("last_unlabeled", QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));
        QFileDialog dialog(this, tr("Select unlabeled data files"));
        dialog.setFileMode(QFileDialog::ExistingFiles);
        dialog.setDirectory(last_unlabeled.toString());
        dialog.setNameFilter("MD compound files (*.cmp)");
        if (dialog.exec()) {
            QStringList files = dialog.selectedFiles();
            for (auto file : files) {
                this->addUnlabeled(file);
            }
            settings->setValue("last_unlabeled", QFileInfo(files[0]).path());
        }
    });
    connect(ui->buttonDelUnlabeled, &QPushButton::clicked, [=]() {
        QString file = ui->listViewUnlabeled->currentIndex().data().toString();
        auto itr = std::find(this->unlabeled_files.begin(), this->unlabeled_files.end(), file);
        this->unlabeled_files.erase(itr);
        modelUnlabeled->removeRows(ui->listViewUnlabeled->currentIndex().row(), 1);
    });
}

void
DialogAddCondition::addLabeled(QString file) {
    if (!labeled_files.contains(file)) {
        this->labeled_files.push_back(file);
        int row = this->modelLabeled->rowCount();
        this->modelLabeled->insertRow(row);
        this->modelLabeled->setData(this->modelLabeled->index(row), file);
    }
}

void
DialogAddCondition::addUnlabeled(QString file) {
    if (!unlabeled_files.contains(file)) {
        this->unlabeled_files.push_back(file);
        int row = this->modelUnlabeled->rowCount();
        this->modelUnlabeled->insertRow(row);
        this->modelUnlabeled->setData(this->modelUnlabeled->index(row), file);
    }
}

void
DialogAddCondition::reset() {
    ui->lineEditName->clear();
    this->modelLabeled->removeRows(0, this->modelLabeled->rowCount());
    this->modelUnlabeled->removeRows(0, this->modelUnlabeled->rowCount());
    this->name = "";
    this->unlabeled_files = QStringList();
    this->labeled_files = QStringList();
    this->row = -1;
}

void
DialogAddCondition::edit(QString name, QStringList labeled, QStringList unlabeled, int row) {
    ui->lineEditName->setText(name);
    this->name = name;
    this->row = row;
    for (auto file : unlabeled) {
        addUnlabeled(file);
    }
    for (auto file : labeled) {
        addLabeled(file);
    }
}


void
DialogAddCondition::accept() {

    if(ui->lineEditName->text() == "") {
        QMessageBox box("MIAMI Error", "Please add condition name!", QMessageBox::NoIcon, QMessageBox::Ok,
                        QMessageBox::NoButton, QMessageBox::NoButton);
        box.exec();
    } else if (this->unlabeled_files.empty() || this->labeled_files.empty()) {
        QMessageBox box("MIAMI Error", "At least one labeled and one unlabeled files are needed!",
                        QMessageBox::NoIcon, QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
        box.exec();
    } else {
        emit addConditionDone(this->name, this->labeled_files, this->unlabeled_files, this->row);
        this->reset();
        this->close();
    }
}

DialogAddCondition::~DialogAddCondition()
{
    delete ui;
}
