// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef DIALOGIMPORT_H
#define DIALOGIMPORT_H

#include <QDialog>
#include <QtWidgets>

#include <gcmsanalyzer.h>
#include <defaultpeakdetector.h>
#include <cdfimporter.h>



namespace Ui {
class DialogImport;
}

class DialogImport : public QDialog
{
    Q_OBJECT

public:
    explicit DialogImport(QWidget *parent = nullptr, QStringList files = QStringList());
    ~DialogImport();

private:
    Ui::DialogImport *ui;
    QList<QString> importFiles(const QList<QString> &files);
    QString importFile(const QString &file);
    void deconvoluteFiles(const QList<QString> &files);
    static void deconvoluteFile(const QString &file);

    QFutureWatcher<QList<QString>> *importFuture;
    QFutureWatcher<void> *deconvolutionFuture;
    QStringListModel *modelFiles;

    struct DeconvolutionWrapper {
        DeconvolutionWrapper():
            baselineCorrection() {}
        void operator()(const QString &file) {
            DialogImport::deconvoluteFile(file);
        }
        bool baselineCorrection;
    };
};

#endif // DIALOGIMPORT_H
