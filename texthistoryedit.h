// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef TEXTHISTORYEDIT_H
#define TEXTHISTORYEDIT_H

#include <QLineEdit>
#include <QWidget>

class TextHistoryEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit TextHistoryEdit(QWidget *parent = nullptr);

private:
    void keyPressEvent(QKeyEvent *e);

    QStringList history;
    int idx = -1;
signals:

public slots:
};

#endif // TEXTHISTORYEDIT_H
