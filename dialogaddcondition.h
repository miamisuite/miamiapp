// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef DIALOGADDCONDITION_H
#define DIALOGADDCONDITION_H

#include <QDialog>
#include <QStringListModel>

namespace Ui {
class DialogAddCondition;
}

class DialogAddCondition : public QDialog
{
    Q_OBJECT

public:
    explicit DialogAddCondition(QWidget *parent = nullptr);
    ~DialogAddCondition();

    QString name;
    QStringList labeled_files;
    QStringList unlabeled_files;
    int row = -1;
    void edit(QString,
              QStringList labeled,
              QStringList unlabeled,
              int row);


signals:
    void addConditionDone(QString,
                          QStringList labeled,
                          QStringList unlabeled,
                          int row);

private:
    QStringListModel *modelLabeled;
    QStringListModel *modelUnlabeled;
    Ui::DialogAddCondition *ui;
    void accept();
    void reset();
    void addLabeled(QString file);
    void addUnlabeled(QString file);
};

#endif // DIALOGADDCONDITION_H
