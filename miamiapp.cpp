// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <QtWidgets>

#include <iomanip>

#include "miamiapp.h"
#include "datalistmodel.h"
#include "miamiwebenginepage.h"
#include "dialogimport.h"
#include "dialogaddcondition.h"
#include "ui_miamiapp.h"
#include "javascriptconsole.h"
#include "dialogricalibrator.h"
#include "statuswidget.h"

#include <miami/miami.hpp>
#include <miami/config.hpp>
#include <miami/tools.hpp>

#ifdef QT_DEBUG
    static bool debug = true;
#endif

#ifdef QT_NO_DEBUG
    static bool debug = false;
#endif


MiamiApp::MiamiApp(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MiamiApp)
{
    ui->setupUi(this);
    ui->webview->setPage(new MiamiWebEnginePage());
    this->flagViz = status::error;
    this->webReady = false;
    connect(ui->webview->page(), &QWebEnginePage::loadFinished, [=](bool ok) {
        if (ok) {
            this->flagViz = status::empty;
            this->webReady = true;
        } else {
            this->flagViz = status::error;
            this->webReady = true;
        }
        this->changeDirtiness(this->flagDirty);
    });

    if (debug) {
        ui->webview->load(QUrl("qrc:/visualization/visualization/index.html?Qt&debug"));
    } else {
        ui->webview->load(QUrl("qrc:/visualization/visualization/index.html?Qt"));
    }

    console = new JavascriptConsole();
    console->setPage(static_cast<MiamiWebEnginePage*>(ui->webview->page()));


    QWebChannel *wc = new QWebChannel(this);
    ui->webview->page()->setWebChannel(wc);
    wc->registerObject("miamiapp", this);


    // Load FontAwesome (if possible)
    this->fa = nullptr;
    if (QFontDatabase::addApplicationFont(":/fonts/fonts/Font Awesome 5 Free-Solid-900.otf") >= 0) {
        this->fa = new QFont("Font Awesome 5 Free", 12, 0, false);
        this->fa->setStyleName("Solid");
    }
    this->config = nullptr;
    this->miami = nullptr;
    this->settings = new QSettings("TU Braunschweig", "Miami");

    this->nodeName = nodeName::metaboliteName;

    this->threadMiami = new QFutureWatcher<void>(this);
    connect(this->threadMiami, SIGNAL(finished()), this, SLOT(miamiFinished()));

    //
    // Init Dialogs
    //

    dialogAddCondition = new DialogAddCondition();
    connect(dialogAddCondition, SIGNAL(addConditionDone(QString, QStringList, QStringList, int)),
            this, SLOT(addCondition(QString, QStringList, QStringList, int)));

    //
    // Init UI elements
    //
    this->initUi();

    //
    // Connect UI elements
    //
    // Menu -> File...
    connect(ui->actionNew, SIGNAL(triggered()), this, SLOT(newFile()));
    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(openFile()));
    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(saveFile()));
    connect(ui->actionSave_As, SIGNAL(triggered()), this, SLOT(saveFileAs()));
    connect(ui->actionExportLibrary, SIGNAL(triggered()), this, SLOT(exportLibrary()));
    connect(ui->actionImport, &QAction::triggered, [=]() {
         DialogImport *diag = new DialogImport();
         diag->show();
    });


    //
    // Menu -> Window...
    connect(ui->actionExperimentDock, &QAction::triggered, [=](){
        ui->dockExperiment->setVisible(ui->actionExperimentDock->isChecked());
    });
    connect(ui->actionLegendDock, &QAction::triggered, [=](){
        ui->dockLegend->setVisible(ui->actionLegendDock->isChecked());
    });
    connect(ui->actionTargetsDock, &QAction::triggered, [=](){
        ui->dockTargets->setVisible(ui->actionTargetsDock->isChecked());
    });
    connect(ui->actionOptionsDock, &QAction::triggered, [=](){
        ui->dockOptions->setVisible(ui->actionOptionsDock->isChecked());
    });
    connect(ui->actionMetabolitesDock, &QAction::triggered, [=](){
        ui->dockMetabolites->setVisible(ui->actionMetabolitesDock->isChecked());
    });
    connect(ui->dockMetabolites, &QDockWidget::visibilityChanged, [=]() {
        ui->actionMetabolitesDock->setChecked(ui->dockMetabolites->isVisible());
    });
    connect(ui->dockLegend, &QDockWidget::visibilityChanged, [=]() {
        ui->actionLegendDock->setChecked(ui->dockLegend->isVisible());
    });
    connect(ui->dockTargets, &QDockWidget::visibilityChanged, [=]() {
        ui->actionTargetsDock->setChecked(ui->dockTargets->isVisible());
    });
    connect(ui->dockExperiment, &QDockWidget::visibilityChanged, [=]() {
        ui->actionExperimentDock->setChecked(ui->dockExperiment->isVisible());
    });
    connect(ui->dockOptions, &QDockWidget::visibilityChanged, [=]() {
        ui->actionOptionsDock->setChecked(ui->dockOptions->isVisible());
    });

    //
    // Menu -> Tools
    connect(ui->actionRiCalibration, &QAction::triggered, [=]() {
        DialogRiCalibrator *diag = new DialogRiCalibrator();
        diag->show();
    });

    connect(ui->actionLibraryImport, &QAction::triggered, [=]() {
        QVariant last_library = this->settings->value("last_library",QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));
        QString filename = QFileDialog::getOpenFileName(this, tr("Select MSL library file"), last_library.toString(), "MSL library (*.msl);;all files (*.*)");


        if (filename != "") {
            try {
                QFutureWatcher<void> *thread = new QFutureWatcher<void>();
                QFileInfo file(filename);
                QString outfile = file.path() + "/" + file.baseName() + ".lbr";

                this->statusRunning->setVisible(true);

                connect(thread, &QFutureWatcher<void>::finished, [=]() {
                    this->statusRunning->setVisible(false);
                    ui->statusBar->showMessage(QString("Library saved as %1").arg(outfile), 5000);
                });

                thread->setFuture(QtConcurrent::run([=]() {
                    auto lib = gcms::LibrarySearch<int, float>::importMSL(filename.toStdString().c_str());
                    lib->toDisk(outfile.toStdString().c_str());
                }));

            } catch (...) {
                ui->statusBar->showMessage("Error during library import!", 5000);
            }
        }
    });

    //
    // Menu -> About...
    connect(ui->actionInfo, &QAction::triggered, [=]() {
       QMessageBox info;

       QString infoText = "<div style=\"text-align: center;\">"
                          "    <img src=\":/icons/logo_miami.png\">"
                          "    <h2>MIAMIapp Version %1</h2>"
                          "    <div style=\"font-size: 0.6ex\">with MIAMIviz Version 1.0.0</div><br>"
                          "</div>"
                          "<div>"
                          "    <label>Homepage:</label> <a href=\"http://miami.tu-bs.de\" target=\"_blank\">MIAMI.tu-bs.de</a>"
                          "</div>"
                          "<div>"
                          "    <label>Contact:</label> Prof. Dr. Karsten Hiller (<a href=\"mailto:karsten.hiller@tu-bs.de\">karsten.hiller@tu-bs.de</a>)"
                          "</div>"
                          "<div>"
                          "    <label>Developer:</label> Christian-Alexander Dudek (<a href=\"mailto:c.dudek@tu-bs.de\">c.dudek@tu-bs.de</a>)"
                          "</div>"
                          "<hr>"
                          "<div>MIAMIviz uses the <a href=\"https://d3js.org\" target=\"_blank\">D3.js library</a> &copy; Mike Bostock</div>";

       info.setWindowTitle("About MIAMIapp");
       info.setIcon(QMessageBox::NoIcon);
       info.setText(infoText.arg(MIAMI_VERSION));

       info.exec();
    });

    //
    // Menu -> About...
    connect(ui->actionHelp, &QAction::triggered, [=]() {
       QMessageBox help;

       QString helpText = "<h1>Help</h1>"
                          "<h3>Node colors:</h3>"
                          "<p>"
                          "    <img src=\":/icons/help_colors.png\">"
                          "</p>"
                          "<h3>Node borders:</h3>"
                          "<p>"
                          "    <img src=\":/icons/help_borders.png\">"
                          "</p>"
                          "<p style=\"font-weight: bold; text-align: right\">"
                          "    <a href=\"http://miami.tu-bs.de/help/index.html\" target=\"_blank\">MIAMI online help</a>"
                          "</p>";

       help.setWindowTitle("MIAMIapp Help");
       help.setIcon(QMessageBox::NoIcon);
       help.setText(helpText);

       help.exec();
    });

    //
    // Menu -> Debug...
    if(!debug) {
        ui->menuDebug->menuAction()->setVisible(false);
    } else {
        connect(ui->actionPrintMiami, &QAction::triggered, [=]() {
            if(this->miami != nullptr) {
                std::cout << std::setw(2) << this->miami->toJson() << std::endl;
            }
        });
        connect(ui->actionPrintConfig, &QAction::triggered, [=]() {
            if(this->config != nullptr) {
                std::cout << std::setw(2) << this->config->toJson() << std::endl;
            }
        });
        connect(ui->actionReloadWebView, &QAction::triggered, [=]() {
           ui->webview->reload();
        });

        connect(ui->webview, &QWebEngineView::loadFinished, [=]() {
            if(this->json != "") {
                QString code = QString("load_json(%1)").arg(this->json);
                ui->webview->page()->runJavaScript(code);
            }
        });
        connect(ui->actionJavascriptConsole, &QAction::triggered, [=]() {
            this->console->show();
            this->console->raise();
            this->console->activateWindow();
        });
    }


    //
    // Options dock
    connect(ui->distanceSlider, &QSlider::valueChanged, [=](int value) {
       double val = value/100.0;
       ui->distanceCutoffLabel->setText(QString::number(val, 'd', 2));
       ui->webview->page()->runJavaScript(QString("config.miami_distance_cutoff = %1").arg(val));
       this->updateUi(true);
    });

    connect(ui->variabilitySlider, &QSlider::valueChanged, [=](int value) {
       double val = value/100.0;
       ui->variabilityCutoffLabel->setText(QString::number(val, 'd', 2));
       ui->webview->page()->runJavaScript(QString("config.miami_variability_cutoff = %1").arg(val));
       this->updateUi(false);
    });

    connect(ui->quantSpin, QOverload<int>::of(&QSpinBox::valueChanged), [=](int value) {
       ui->webview->page()->runJavaScript(QString("config.miami_quant_cutoff = %1").arg(value));
       this->updateUi(true);
    });

    connect(ui->comboPathStart, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](){
        this->updatePath();
    });
    connect(ui->comboPathEnd, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](){
        this->updatePath();
    });

    connect(ui->comboVariability1, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](){
        QString code = QString("select_experiments('%1', '%2')").arg(ui->comboVariability1->currentText()).arg(ui->comboVariability2->currentText());
        ui->webview->page()->runJavaScript(code);
        this->updateUi(false);

    });
    connect(ui->comboVariability2, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](){
        QString code = QString("select_experiments('%1', '%2')").arg(ui->comboVariability1->currentText()).arg(ui->comboVariability2->currentText());
        ui->webview->page()->runJavaScript(code);
        this->updateUi(false);
    });

    connect(ui->checkShowArrows, SIGNAL(clicked()), this, SLOT(toggleShowArrows()));
    connect(ui->checkRemoveEdges, SIGNAL(clicked()), this, SLOT(toggleRemoveEdges()));
    connect(ui->checkShowTargets, SIGNAL(clicked()), this, SLOT(toggleShowTargets()));
    connect(ui->checkShowReferenceNodes, SIGNAL(clicked()), this, SLOT(toggleShowRefereceNodes()));
    connect(ui->checkEdgesOverNodes, SIGNAL(clicked()), this, SLOT(toggleEdgesOverNodes()));
    connect(ui->checkShowSingleNodes, SIGNAL(clicked()), this, SLOT(toggleShowSingleNodes()));

    ui->comboNodeName->clear();
    ui->comboNodeName->addItem("Name", nodeName::metaboliteName);
    ui->comboNodeName->addItem("RI", nodeName::ri);
    ui->comboNodeName->addItem("RT [min]", nodeName::rtMin);
    ui->comboNodeName->addItem("RT [s]", nodeName::rtSec);
    connect(ui->comboNodeName, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int idx) {
        this->changeNodeName(ui->comboNodeName->itemData(idx).toInt());
    });

    //
    // Targets dock
    connect(ui->listTargets, &QListView::clicked, [=]() {
        auto idx = ui->listTargets->currentIndex();
        int edge_idx = this->modelTargets->data(idx, Qt::UserRole).toInt();
        QString code = QString("move2target(%1);").arg(edge_idx);
        ui->webview->page()->runJavaScript(code);
        ui->buttonTargetToCsv->setEnabled(true);
    });

    connect(ui->buttonAllTargetsToCsv, &QPushButton::clicked, [=]() {
        ui->webview->page()->runJavaScript("targets2csv()", [=](QVariant data) {
            QString default_dir;
            if (this->current_file.isEmpty()) {
                default_dir = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first();
            } else {
                default_dir = this->current_file;
            }
            QVariant savePath = settings->value("savePath", default_dir);
            QString filename = QFileDialog::getSaveFileName(this, tr("Save all targets as CSV"), savePath.toString(), "csv files (*.csv)");
            std::ofstream os(filename.toStdString());
            os << data.toString().toStdString();
            os.close();
        });
    });

    //
    // Experiment dock

    connect(ui->buttonAddCondition, &QPushButton::clicked, [=]() {
        dialogAddCondition->exec();
    });
    connect(ui->buttonDelCondition, SIGNAL(clicked()), this, SLOT(delCondition()));
    connect(ui->buttonEditCondition, SIGNAL(clicked()), this, SLOT(editCondition()));

    connect(ui->buttonAddLibrary, SIGNAL(clicked()), this, SLOT(selectLibraries()));
    connect(ui->buttonDelLibrary, SIGNAL(clicked()), this, SLOT(delLibrary()));

    connect(ui->buttonAddPathway, SIGNAL(clicked()), this, SLOT(selectPathways()));
    connect(ui->buttonDelPathway, SIGNAL(clicked()), this, SLOT(delPathway()));

    connect(ui->buttonRun, SIGNAL(clicked()), this, SLOT(miamiStart()));

    connect(ui->listLibraries, &QListView::clicked, [=](){
        ui->buttonDelLibrary->setEnabled(true);
    });
    connect(this->modelLibraries, &QStringListModel::rowsRemoved, [=]() {
        if (this->modelLibraries->rowCount() == 0) {
            ui->buttonDelLibrary->setEnabled(false);
        }
    });
    connect(ui->listPathways, &QListView::clicked, [=](){
        ui->buttonDelPathway->setEnabled(true);
    });
    connect(this->modelPathways, &QStringListModel::rowsRemoved, [=]() {
        if (this->modelPathways->rowCount() == 0) {
            ui->buttonDelPathway->setEnabled(false);
        }
    });
    connect(ui->listConditions, &QListView::clicked, [=](){
        ui->buttonDelCondition->setEnabled(true);
        ui->buttonEditCondition->setEnabled(true);
    });
    connect(this->modelConditions, &QStringListModel::rowsRemoved, [=]() {
        if (this->modelConditions->rowCount() == 0) {
            ui->buttonDelCondition->setEnabled(false);
            ui->buttonEditCondition->setEnabled(false);
        }
    });

    connect(ui->buttonOpenNist, &QPushButton::clicked, [=]() {
        QVariant last_library = this->settings->value("last_library",QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));
        QVariant last_nist = this->settings->value("last_nist", last_library);
        QString filename = QFileDialog::getOpenFileName(this, tr("Select NIST library file"), last_nist.toString(), "NIST SQLite file (*.lbr);;all files (*.*)");

        if (filename != "") {
            this->config->library_nist = filename.toStdString();
            ui->lineEditNist->setText(filename);
            this->settings->setValue("last_nist", filename);
            this->settings->sync();
            this->changeDirtiness(true);
        }
    });
    connect(ui->buttonDelNist, &QPushButton::clicked, [=]() {
        this->config->library_nist = "";
        ui->lineEditNist->clear();
        this->changeDirtiness(true);
    });

    connect(ui->comboTracer, QOverload<const QString&>::of(&QComboBox::currentTextChanged), [=](QString tracer){
        this->config->tracer = tracer.toStdString();
        this->changeDirtiness(true);
    });

    connect(ui->spinIdentification, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double value){
        this->config->identification_cutoff = value;
        this->changeDirtiness(true);
    });

    connect(ui->checkReferenceFull, &QCheckBox::stateChanged, [=](){
        this->config->reference_full = ui->checkReferenceFull->isChecked();
        this->changeDirtiness(true);
    });

    connect(ui->checkReferenceShortest, &QCheckBox::stateChanged, [=](){
        this->config->reference_shortest = ui->checkReferenceShortest->isChecked();
        this->changeDirtiness(true);
    });

    connect(ui->spinRefererenceDepth, QOverload<int>::of(&QSpinBox::valueChanged), [=](int value){
        this->config->reference_depth = value;
        this->changeDirtiness(true);
    });

    connect(ui->textReferenceRemove, &QPlainTextEdit::textChanged, [=]() {
        // NOTE: This is maybe not the most elegant solution, but seems to
        //       have no negative impact on performance.
        this->config->reference_remove.clear();
        for (auto metabolite : ui->textReferenceRemove->toPlainText().split("\n")) {
            if (metabolite.simplified() != "") {
                this->config->reference_remove.push_back(metabolite.simplified().toStdString());
            }
        }
        this->changeDirtiness(true);
    });

    connect(ui->spinLabelingMin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double value){
        this->config->labid_min_labeling = value;
        this->changeDirtiness(true);
    });
    connect(ui->spinLabelingMax, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double value){
        this->config->labid_max_labeling = value;
        this->changeDirtiness(true);
    });
    connect(ui->spinIsotopeError, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double value){
        this->config->miami_error_per_isotopomer = value;
        this->changeDirtiness(true);
    });

    connect(ui->comboDistanceMethod, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int method){
        switch (method) {
        case 0:
            this->config->miami_distance_method = miami_tools::Distance::Euclidean;
        break;
        case 1:
            this->config->miami_distance_method = miami_tools::Distance::Canberra;
        break;
        case 2:
            this->config->miami_distance_method = miami_tools::Distance::Manhattan;
        break;
        default:
            this->config->miami_distance_method = miami_tools::Distance::Canberra;
        break;
        }

        this->changeDirtiness(true);
    });

    connect(ui->comboDistanceNorm, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int method){
        switch (method) {
        case 0:
            this->config->miami_distance_norm = miami_tools::Normalization::None;
        break;
        case 1:
            this->config->miami_distance_norm = miami_tools::Normalization::Sum;
        break;
        case 2:
            this->config->miami_distance_norm = miami_tools::Normalization::Prod;
        break;
        case 3:
            this->config->miami_distance_norm = miami_tools::Normalization::Max;
        break;
        case 4:
            this->config->miami_distance_norm = miami_tools::Normalization::Min;
        break;
        default:
            this->config->miami_distance_norm = miami_tools::Normalization::None;
        break;
        }

        this->changeDirtiness(true);
    });

    //
    // Metabolites dock
    connect(ui->listMetabolites, &QListView::clicked, [=]() {
        auto idx = ui->listMetabolites->currentIndex();
        QStringList data = this->modelMetabolites->data(idx, Qt::UserRole).toStringList();
        QString code = QString("node_click(%1)").arg(data[0].toInt());
        ui->webview->page()->runJavaScript(code);
        ui->buttonMetToCsv->setEnabled(true);
        ui->buttonMetToSvg->setEnabled(true);
    });

    connect(ui->buttonAllMetToCsv, &QPushButton::clicked, [=]() {
        ui->webview->page()->runJavaScript("mids2csv()", [=](QVariant data) {
            QString default_dir;
            if (this->current_file.isEmpty()) {
                default_dir = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first();
            } else {
                default_dir = this->current_file;
            }
            QVariant savePath = settings->value("savePath", default_dir);
            QString filename = QFileDialog::getSaveFileName(this, tr("Save all MIDs as CSV"), savePath.toString(), "csv files (*.csv)");
            std::ofstream os(filename.toStdString());
            os << data.toString().toStdString();
            os.close();
        });
    });

    connect(ui->buttonMetToCsv, &QPushButton::clicked, [=]() {
        auto idx = ui->listMetabolites->currentIndex();
        QStringList data = this->modelMetabolites->data(idx, Qt::UserRole).toStringList();
        QString code = QString("mids2csv(%1)").arg(data[0].toInt());
        ui->webview->page()->runJavaScript(code, [=](QVariant data) {
            QString default_dir;
            if (this->current_file.isEmpty()) {
                default_dir = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first();
            } else {
                default_dir = this->current_file;
            }
            QVariant savePath = settings->value("savePath", default_dir);
            QString filename = QFileDialog::getSaveFileName(this, tr("Save MIDs as CSV"), savePath.toString(), "csv files (*.csv)");
            std::ofstream os(filename.toStdString());
            os << data.toString().toStdString();
            os.close();
        });
    });

    connect(ui->buttonMetToSvg, &QPushButton::clicked, [=]() {
        auto idx = ui->listMetabolites->currentIndex();
        QStringList data = this->modelMetabolites->data(idx, Qt::UserRole).toStringList();
        QString code = QString("mids2svg(%1)").arg(data[0].toInt());
        ui->webview->page()->runJavaScript(code, [=](QVariant data) {
            QString default_dir;
            if (this->current_file.isEmpty()) {
                default_dir = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first();
            } else {
                default_dir = this->current_file;
            }
            QVariant savePath = settings->value("savePath", default_dir);
            QString filename = QFileDialog::getSaveFileName(this, tr("Save MIDs as SVG"), savePath.toString(), "svg files (*.svg)");
            std::ofstream os(filename.toStdString());
            os << data.toString().toStdString();
            os.close();
        });
    });
}

void
MiamiApp::setFa(QPushButton *button, QString icon) {
    button->setFont(*this->fa);
    button->setText(icon);
    button->setFixedSize(25, 25);
};

void
MiamiApp::changeDirtiness() {
    this->changeDirtiness(!this->flagDirty);
}

void
MiamiApp::changeDirtiness(bool state) {
    // Set application flags
    this->flagDirty = state;
    this->flagConf = status::ok;
    this->flagViz = status::empty;
    this->flagAnalysis = status::empty;

    ui->actionExportGraph->setEnabled(false);
    ui->actionExportLibrary->setEnabled(false);
    ui->actionExportTargets->setEnabled(false);
    ui->actionExportMetabolites->setEnabled(false);

    // Set window title
    QString title = "MIAMI - %1%2";
    if (this->current_file.isEmpty()) {
        title = title.arg("untitled");
    } else {
        QFileInfo f(this->current_file);
        title = title.arg(f.baseName());
    }
    if (this->flagDirty) {
        this->window()->setWindowTitle(title.arg("*"));
    } else {
        this->window()->setWindowTitle(title.arg(""));
    }

    // Update status

    if(this->miami != nullptr && this->miami->isDone()) {
        this->flagAnalysis = status::ok;
        ui->actionExportLibrary->setEnabled(true);
    }

    if (this->flagViz != status::error) {
        if(this->webReady) {
            ui->webview->page()->runJavaScript("check_data()", [this](const QVariant &v) {
                if(v.toBool()) {
                    this->flagViz = status::ok;
                    ui->actionExportGraph->setEnabled(true);
                    ui->actionExportMetabolites->setEnabled(true);
                    ui->actionExportTargets->setEnabled(true);
                }
                statusViz->setStatus(this->flagViz);
            });
        }

    }
    if(this->config_hash != size_t(-1) && this->config_hash != this->config->hash()) {
        this->flagConf = status::changed;
    }
    if(this->miami != nullptr && (!this->miami->checkFiles().empty() || this->miami->getExperiments().empty())) {
        ui->buttonRun->setEnabled(false);
        this->flagConf = status::error;
    } else if(this->miami != nullptr) {
        ui->buttonRun->setEnabled(true);
    }

    statusConf->setStatus(this->flagConf);
    statusAnalysis->setStatus(this->flagAnalysis);
}


void
MiamiApp::initUi() {
    //
    // Statusbar
    //
    statusConf = new StatusWidget("Configuration");
    statusAnalysis = new StatusWidget("Analysis");
    statusViz = new StatusWidget("Visualization");

    statusNodes = new StatusWidget("Nodes");

    statusEdges = new StatusWidget("Edges");
    statusTargets = new StatusWidget("Targets");

    if (this->fa != nullptr) {
        this->fa->setWeight(900);
        this->fa->setPixelSize(14);

        this->setFa(ui->buttonAddLibrary, "\uf067");
        this->setFa(ui->buttonDelLibrary, "\uf068");

        this->setFa(ui->buttonAddCondition, "\uf067");
        this->setFa(ui->buttonDelCondition, "\uf068");
        this->setFa(ui->buttonEditCondition, "\uf304");

        this->setFa(ui->buttonAddPathway, "\uf067");
        this->setFa(ui->buttonDelPathway, "\uf068");

        this->setFa(ui->buttonAllTargetsToCsv, "\uf15c");
        this->setFa(ui->buttonTargetToCsv, "\uf15b");

        this->setFa(ui->buttonMetToCsv, "\uf15b");
        this->setFa(ui->buttonMetToSvg, "\uf1c5");
        this->setFa(ui->buttonAllMetToCsv, "\uf15c");

        this->setFa(ui->buttonOpenNist, "\uf07c");
        this->setFa(ui->buttonDelNist, "\uf1f8");
    }

    statusRunning = new QWidget();
    statusRunning->setMaximumWidth(150);

    QProgressBar *bar = new QProgressBar();
    QHBoxLayout *ly = new QHBoxLayout();
    QLabel *l = new QLabel("Running...");

    bar->setMaximumWidth(100);
    bar->setMinimum(0);
    bar->setMaximum(0);
    ly->addWidget(bar);
    ly->addWidget(l);

    statusRunning->setLayout(ly);
    statusRunning->setVisible(false);

    ui->statusBar->addPermanentWidget(statusNodes);
    ui->statusBar->addPermanentWidget(statusEdges);
    ui->statusBar->addPermanentWidget(statusTargets);
    ui->statusBar->addPermanentWidget(statusRunning);
    ui->statusBar->addPermanentWidget(statusViz);
    ui->statusBar->addPermanentWidget(statusConf);

    legend_layout = nullptr;

    // Ui elements

    ui->distanceSlider->setTickInterval(1);
    ui->distanceSlider->setMinimum(0);
    ui->distanceSlider->setMaximum(100);

    ui->variabilitySlider->setTickInterval(1);
    ui->variabilitySlider->setMinimum(0);
    ui->variabilitySlider->setMaximum(100);

    // List Models
    this->modelLibraries = new QStringListModel();
    this->modelLibraries->setStringList(QStringList());
    ui->listLibraries->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listLibraries->setModel(modelLibraries);

    modelPathways = new QStringListModel();
    modelPathways->setStringList(QStringList());
    ui->listPathways->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listPathways->setModel(modelPathways);

    modelConditions = new DataListModel();
    ui->listConditions->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listConditions->setModel(modelConditions);

    this->modelMetabolites = new DataListModel();
    ui->listMetabolites->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listMetabolites->setModel(modelMetabolites);

    this->modelTargets = new DataListModel();
    ui->listTargets->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listTargets->setModel(modelTargets);

    // Init distance and normalization
    ui->comboDistanceMethod->addItem("Euclidean", miami_tools::Distance::Euclidean);
    ui->comboDistanceMethod->addItem("Manhattan", miami_tools::Distance::Manhattan);
    ui->comboDistanceMethod->addItem("Canberra", miami_tools::Distance::Canberra);

    ui->comboDistanceNorm->addItem("None", miami_tools::Normalization::None);
    ui->comboDistanceNorm->addItem("Sum", miami_tools::Normalization::Sum);
    ui->comboDistanceNorm->addItem("Prod", miami_tools::Normalization::Prod);
    ui->comboDistanceNorm->addItem("Max", miami_tools::Normalization::Max);
    ui->comboDistanceNorm->addItem("Min", miami_tools::Normalization::Min);

    // Init isotope tracer
    QList<QString> tracers = {"C", "N", "H", "O", "S", "P"};
    for (auto t : tracers) {
        ui->comboTracer->addItem(t, t);
    }

    ui->spinIdentification->setDecimals(2);
    ui->spinIdentification->setSingleStep(0.01);
    ui->spinIdentification->setRange(0.0, 1.0);

    ui->spinLabelingMin->setDecimals(2);
    ui->spinLabelingMin->setSingleStep(0.01);
    ui->spinLabelingMin->setRange(0.0, 1.0);

    ui->spinLabelingMax->setDecimals(2);
    ui->spinLabelingMax->setSingleStep(0.01);
    ui->spinLabelingMax->setRange(0.0, 1.0);

    this->resetUi();
}

void
MiamiApp::initExperiments() {
    QString style = "QCheckBox:unchecked { color: grey; }" \
                    "QCheckBox::indicator {width: 0.7em; height: 0.7em; border: 1px solid #333;}" \
                    "QCheckBox::indicator:unchecked {background-color: grey; }" \
                    "QCheckBox::indicator:checked { background-color: %1; }" \
                    "QCheckBox { color: %1 }";

    ui->comboVariability1->clear();
    ui->comboVariability2->clear();

    this->resetLegend();
    ui->webview->page()->runJavaScript("get_experiments()", [=](QVariant experiments) {
        for (auto exp : experiments.toList()) {
            QCheckBox *cb = new QCheckBox();
            cb->setChecked(true);
            QString exp_name = exp.toList()[0].toString();
            QString exp_color = exp.toList()[1].toString();
            cb->setText(exp_name);
            cb->setObjectName(exp_name);
            cb->setStyleSheet(style.arg(exp_color));
            legend_layout->addWidget(cb);
            connect(cb, &QCheckBox::stateChanged, [=]() {
                ui->webview->page()->runJavaScript(QString("experiment_click('%1')").arg(cb->objectName()));
                this->updateUi(true);
            });
            ui->comboVariability1->addItem(exp_name);
            ui->comboVariability2->addItem(exp_name);
        }
        ui->comboVariability1->setCurrentIndex(0);
        ui->comboVariability2->setCurrentIndex(1);
        legend_layout->addSpacerItem(new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed));
    });
}

void
MiamiApp::resetLegend() {
    // Legend
    // Remove Legend items
     QLayoutItem *child;
     if (legend_layout != nullptr) {
         while ((child = legend_layout->takeAt(0)) != nullptr) {
             delete child->widget();
             delete child;
         }
         delete legend_layout;
     }
    legend_layout = new QHBoxLayout();
    ui->dockLegendContents->setLayout(legend_layout);
}

void
MiamiApp::resetUi() {
    this->current_file = "";


    // MIAMI object
    if (miami != nullptr) {
        delete this->miami;
        this->miami = nullptr;
    }

    this->config_hash = size_t(-1);
    this->config = new miami::Config();
    this->miami = new miami::Miami(config);

    // Statusbar
    statusNodes->setVisible(false);
    statusEdges->setVisible(false);
    statusTargets->setVisible(false);

    this->resetLegend();

    // Init ui elements
    ui->checkShowArrows->setChecked(false);
    ui->checkRemoveEdges->setChecked(false);
    ui->checkShowTargets->setChecked(false);
    ui->checkShowReferenceNodes->setChecked(true);
    ui->checkEdgesOverNodes->setChecked(false);
    ui->checkShowSingleNodes->setChecked(false);


    ui->dockLegend->setVisible(false);

    tabifyDockWidget(ui->dockExperiment, ui->dockOptions);
    ui->dockExperiment->raise();
    ui->dockOptions->setVisible(false);
    ui->dockExperiment->setVisible(true);

    tabifyDockWidget(ui->dockMetabolites, ui->dockTargets);
    ui->dockMetabolites->setVisible(false);
    ui->dockTargets->setVisible(false);

    ui->distanceSlider->setValue(int(config->miami_distance_cutoff*100));
    ui->distanceCutoffLabel->setText(QString::number(config->miami_distance_cutoff, 'd', 2));

    ui->variabilitySlider->setValue(int(config->miami_variability_cutoff*100));
    ui->variabilityCutoffLabel->setText(QString::number(config->miami_variability_cutoff, 'd', 2));

    ui->comboVariability1->clear();
    ui->comboVariability2->clear();

    ui->comboPathStart->clear();
    ui->comboPathEnd->clear();

    ui->comboPathStart->addItem("", -1);
    ui->comboPathEnd->addItem("", -1);

    // Reset list views
    this->modelConditions->removeRows(0, this->modelConditions->rowCount());
    this->modelLibraries->removeRows(0, this->modelLibraries->rowCount());
    this->modelPathways->removeRows(0, this->modelPathways->rowCount());
    this->modelMetabolites->removeRows(0, this->modelMetabolites->rowCount());
    this->modelTargets->removeRows(0, this->modelTargets->rowCount());

    this->configToUi();

    ui->buttonMetToCsv->setEnabled(false);
    ui->buttonMetToSvg->setEnabled(false);
    ui->buttonTargetToCsv->setEnabled(false);

//    ui->webview->page()->runJavaScript("clear_json()");
    changeDirtiness(false);
}

void
MiamiApp::configToUi() {
    for (auto exp : this->miami->getExperiments()) {
        QStringList lab;
        for (auto f : exp->getLabeledFiles()) {
            lab << QString::fromStdString(f);
        }
        QStringList ulab;
        for (auto f : exp->getUnlabeledFiles()) {
            ulab << QString::fromStdString(f);
        }
        this->addCondition(QString::fromStdString(exp->getName()), lab, ulab, -1);
    }

    ui->comboTracer->setCurrentText(QString::fromStdString(this->config->tracer));

    QStringList files;
    for (auto file : this->config->libraries) {
        files << QString::fromStdString(file);
    }
    modelLibraries->setStringList(files);
    files = QStringList();

    ui->lineEditNist->setText(QString::fromStdString(this->config->library_nist));

    ui->spinIdentification->setValue(this->config->identification_cutoff);

    for (auto file : this->config->reference_files) {
        files << QString::fromStdString(file);
    }
    modelPathways->setStringList(files);

    ui->checkReferenceFull->setChecked(this->config->reference_full);
    ui->checkReferenceShortest->setChecked(this->config->reference_shortest);
    ui->spinRefererenceDepth->setValue(config->reference_depth);

    QString metabolites;
    for (auto met : config->reference_remove) {
        metabolites += QString::fromStdString(met);
        if (met != config->reference_remove[config->reference_remove.size()]) {
            metabolites += "\n";
        }
    }
    ui->textReferenceRemove->setPlainText(metabolites);

    ui->spinLabelingMin->setValue(config->labid_min_labeling);
    ui->spinLabelingMax->setValue(config->labid_max_labeling);
    ui->spinIsotopeError->setValue(config->miami_error_per_isotopomer);

    ui->comboDistanceMethod->setCurrentText(QString::fromStdString(config->toJson()["miami_distance_method"].get<std::string>()));
    ui->comboDistanceNorm->setCurrentText(QString::fromStdString(config->toJson()["miami_distance_norm"].get<std::string>()));

    ui->distanceSlider->setValue(int(config->miami_distance_cutoff*100));
    ui->distanceCutoffLabel->setText(QString::number(config->miami_distance_cutoff, 'd', 2));
    ui->quantSpin->setValue(config->miami_quant_cutoff);
    ui->variabilitySlider->setValue(int(config->miami_variability_cutoff*100));
    ui->variabilityCutoffLabel->setText(QString::number(config->miami_variability_cutoff, 'd', 2));

    // TODO (chdudek): Add node_name
    ui->checkShowArrows->setChecked(this->config->miami_show_arrows);
    ui->checkRemoveEdges->setChecked(this->config->miami_remove_edges);
    ui->checkShowTargets->setChecked(this->config->miami_show_targets);
    ui->checkEdgesOverNodes->setChecked(this->config->miami_nodes_over_edges);
    ui->checkShowSingleNodes->setChecked(this->config->miami_single_nodes);
    ui->checkShowReferenceNodes->setChecked(this->config->miami_show_references);
}

void
MiamiApp::miamiStart() {
    if(miami->getExperiments().empty()) {
        QMessageBox::critical(this, "MIAMI Error", "At least one condition is needed!");
        return;
    }
    if(!miami->checkFiles().empty()) {
        QMessageBox::critical(this, "MIAMI Error", "Some files were not found!");
        return;
    }

    if (this->threadMiami->isRunning()) {
        threadMiami->cancel();
        threadMiami->waitForFinished();
    }

    this->config_hash = this->config->hash();

    this->statusRunning->setVisible(true);
    ui->dockExperiment->setEnabled(false);
    threadMiami->setFuture(QtConcurrent::run([=](){
        this->miami->run();
    }));
}



void
MiamiApp::miamiFinished() {
    std::string json = miami->toJson().dump();
    this->json = QString::fromStdString(json);

    QString code = QString("load_json(%1)").arg(this->json);
    ui->webview->page()->runJavaScript(code, [=](QVariant v) {

        this->statusRunning->setVisible(false);
        ui->dockExperiment->setEnabled(true);

        this->initExperiments();
        this->updateUi(false);

    //    ui->dockExperiment->setVisible(false);

        ui->dockLegend->setVisible(true);
        ui->dockOptions->setVisible(true);
        ui->dockOptions->raise();
        ui->dockMetabolites->setVisible(true);
        ui->dockTargets->setVisible(true);
        this->changeDirtiness(true);
    });

}

void
MiamiApp::newFile() {
    if (this->flagDirty &&  QMessageBox::question(this, "MIAMI Save changes?", "There are unsaved changes!\nSave changes now?", QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
        this->saveFile();
    }

    delete miami;
    delete config;

    config = new miami::Config();
    miami = new miami::Miami(config);

    this->resetUi();
    ui->webview->page()->runJavaScript("clear_json()");

}

void
MiamiApp::openFile() {
    if (this->flagDirty &&  QMessageBox::question(this, "MIAMI Save changes?", "There are unsaved changes!\nSave changes now?", QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
        this->saveFile();
    }
    QVariant openPath = settings->value("openPath", QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));
    QString filename = QFileDialog::getOpenFileName(this, tr("Select data file"), openPath.toString(), "json files (*.json)");

    QFile f(filename);
    if (f.open(QFile::ReadOnly | QFile::Text)) {
        this->resetUi();
        this->miami->loadJson(filename.toStdString());

        std::vector<std::string> not_found = miami->checkFiles();

        if (!not_found.empty()) {
            QErrorMessage *msgBox = new QErrorMessage(this);
            msgBox->setWindowTitle("MIAMI Warning");
            QString msg = "<b>Visualization only!</b><br>Some files were not found:";

            for (const auto& file : not_found) {
                msg += QString("<br>%1").arg(QString::fromStdString(file));
            }
            msgBox->showMessage(msg, "visualization_only");
        }
        this->current_file = filename;

        QTextStream json(&f);
        this->json = json.readAll();

        ui->webview->page()->runJavaScript(QString("load_json(%1)").arg(this->json));

        // Save new path to config and sync
        this->settings->setValue("openPath", filename);
        this->settings->sync();

        initExperiments();

//        ui->dockExperiment->setVisible(false);
        ui->dockLegend->setVisible(true);
        ui->dockOptions->setVisible(true);
        ui->dockOptions->raise();
        ui->dockMetabolites->setVisible(true);
        ui->dockTargets->setVisible(true);

        this->updateUi(false);
        this->configToUi(); //TODO
        this->config_hash = this->config->hash();
        this->changeDirtiness(false);
    }
}

void
MiamiApp::saveFile() {
    if (this->current_file.isEmpty()) {
        saveFileAs();
    } else {
        if (this->flagViz == status::ok) {
            qDebug() << "Using viz data";
            ui->webview->page()->runJavaScript("data2string()", [=](QVariant data) {
                std::ofstream os(this->current_file.toStdString());
                os << data.toString().toStdString();
                os.close();
            });
        } else {
            qDebug() << "Using miami data";
            this->miami->saveJson(this->current_file.toStdString());
        }
    }
    this->changeDirtiness(false);
}

void
MiamiApp::saveFileAs() {
    QString default_dir;
    if (this->current_file.isEmpty()) {
        default_dir = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first();
    } else {
        default_dir = this->current_file;
    }
    QVariant savePath = settings->value("savePath", default_dir);
    QString filename = QFileDialog::getSaveFileName(this, tr("Save file as"), savePath.toString(), "json files (*.json)");

    if (!filename.isEmpty()) {
        this->current_file = filename;
        this->saveFile();
    }
}

void
MiamiApp::exportLibrary() {
    QVariant savePath = settings->value("savePath", QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));
    QString saveName = savePath.toString();
    if(this->current_file != "") {
        saveName += this->current_file;
    }
    QString filename = QFileDialog::getSaveFileName(this, tr("Save library"), saveName, "MD Library (*.lbr);;MSL Library (*.msl)");

    QFileInfo f(filename);
    if(f.suffix() == "lbr") {
        this->miami->getLibrary()->toDisk(filename.toStdString().c_str());
    } else {
        this->miami->getLibrary()->exportMSL(filename.toStdString().c_str());
    }
    ui->statusBar->showMessage(QString("Library exported as %1!").arg(filename), 5000);
}


void
MiamiApp::addCondition(QString name, QStringList labeled, QStringList unlabeled, int row) {
    QString display = QString::fromStdString("%1 (%2 labeled, %3 unlabeled)").arg(name).arg(labeled.size()).arg(unlabeled.size());

    // Prepare data
    std::vector<std::string> lab;
    for (auto f : labeled) {
        lab.push_back(f.toStdString());
    }
    std::vector<std::string> ulab;
    for (auto f : unlabeled) {
        ulab.push_back(f.toStdString());
    }

    if (row == -1) {
        row = this->modelConditions->rowCount();
        this->modelConditions->insertRow(row);
        this->miami->addExperiment(name.toStdString(), lab, ulab);
    } else {
        this->miami->changeExperiment(row, name.toStdString(), lab, ulab);
    }

    QMap<QString, QVariant> data;
    data["name"] = name;
    data["labeled"] = labeled;
    data["unlabeled"] = unlabeled;
    this->modelConditions->setData(this->modelConditions->index(row), display, Qt::DisplayRole);
    this->modelConditions->setData(this->modelConditions->index(row), data, Qt::UserRole);
    this->changeDirtiness(true);
}

void
MiamiApp::delCondition() {
    int row = ui->listConditions->currentIndex().row();
    miami->removeExperiment(row);
    modelConditions->removeRows(row, 1);
    this->changeDirtiness(true);
};

void
MiamiApp::editCondition() {
    int row = ui->listConditions->currentIndex().row();
//    std::string name = condition_names[size_t(row)];
    QMap<QString, QVariant> data = this->modelConditions->data(this->modelConditions->index(row), Qt::UserRole).toMap();

    QString name = data["name"].toString();
    QStringList lab = data["labeled"].toStringList();
    QStringList ulab = data["unlabeled"].toStringList();

    dialogAddCondition->edit(name, lab, ulab, row);
    dialogAddCondition->exec();
    this->changeDirtiness(true);
};

void
MiamiApp::addLibrary(QString filename) {
    auto itr = std::find(this->config->libraries.begin(), this->config->libraries.end(), filename.toStdString());
    if(itr == this->config->libraries.end()) {
      int row = modelLibraries->rowCount();
      modelLibraries->insertRow(row);
      modelLibraries->setData(modelLibraries->index(row), filename);
      this->config->addLibrary(filename.toStdString());
      this->changeDirtiness(true);
  }
}

void
MiamiApp::selectLibraries() {
    QVariant last_library = this->settings->value("last_library", QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));

    QFileDialog dialog(this, tr("Select library files"));
    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setDirectory(last_library.toString());
    dialog.setNameFilter("MD Library (*.lbr)");

    if (dialog.exec()) {
        QStringList files = dialog.selectedFiles();
        for (auto file : files) {
            this->addLibrary(file);
        }
        this->settings->setValue("last_library", QFileInfo(files[0]).path());
        this->settings->sync();
        this->changeDirtiness(true);
    }
};

void
MiamiApp::delLibrary() {
    std::string lib = ui->listLibraries->currentIndex().data().toString().toStdString();
    modelLibraries->removeRows(ui->listLibraries->currentIndex().row(), 1);
    auto itr = std::find(this->config->libraries.begin(), this->config->libraries.end(), lib);
    this->config->libraries.erase(itr);
    this->changeDirtiness(true);
};


void
MiamiApp::addPathway(QString filename) {
    auto itr = std::find(this->config->reference_files.begin(), this->config->reference_files.end(), filename.toStdString());
    if(itr == this->config->reference_files.end()) {
        int row = modelPathways->rowCount();
        modelPathways->insertRow(row);
        modelPathways->setData(modelPathways->index(row), filename);
        this->config->reference_files.push_back(filename.toStdString());
        this->changeDirtiness(true);
    }
}

void
MiamiApp::selectPathways() {
    QVariant last_pathway = this->settings->value("last_pathway", QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));

    QFileDialog dialog(this, tr("Select reference pathways"));
    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setDirectory(last_pathway.toString());
    dialog.setNameFilter("Reference pathway (*.json)");

    if (dialog.exec()) {
        QStringList files = dialog.selectedFiles();
        for (auto file : files) {
            this->addPathway(file);
        }
        this->settings->setValue("last_pathway", QFileInfo(files[0]).path());
        this->settings->sync();
        this->changeDirtiness(true);
    }
};

void
MiamiApp::delPathway() {
    std::string pathway = ui->listPathways->currentIndex().data().toString().toStdString();
    modelPathways->removeRows(ui->listPathways->currentIndex().row(), 1);
    auto itr = std::find(this->config->reference_files.begin(), this->config->reference_files.end(), pathway);
    this->config->reference_files.erase(itr);
    this->changeDirtiness(true);
};

void
MiamiApp::toggleShowSingleNodes() {
    QString code;

    if (ui->checkShowSingleNodes->isChecked()) {
        code = "config.miami_single_nodes = true";
    } else {
        code = "config.miami_single_nodes = false";
    }
    ui->webview->page()->runJavaScript((code),[=](QVariant v) {
        this->updateUi(true);
    });

}

void
MiamiApp::toggleShowTargets() {
    ui->webview->page()->runJavaScript(
        QString("config.miami_show_targets = %1").arg(ui->checkShowTargets->isChecked())
    );
    this->updateUi(false);
}

void
MiamiApp::toggleEdgesOverNodes() {
    ui->webview->page()->runJavaScript(
        QString("config.miami_nodes_over_edges = %1").arg(ui->checkEdgesOverNodes->isChecked())
    );
    this->updateUi(false);
}

void
MiamiApp::toggleShowArrows() {
    ui->webview->page()->runJavaScript(
        QString("config.miami_show_arrows = %1").arg(ui->checkShowArrows->isChecked())
    );
    this->updateUi(false);
}

void
MiamiApp::toggleShowRefereceNodes() {
    ui->webview->page()->runJavaScript(
        QString("config.miami_show_references = %1").arg(ui->checkShowReferenceNodes->isChecked())
    );
    this->updateUi(false);
}

void
MiamiApp::toggleRemoveEdges() {
    ui->webview->page()->runJavaScript(
        QString("config.miami_remove_edges = %1").arg(ui->checkRemoveEdges->isChecked())
    );
    this->updateUi(false);
}


void
MiamiApp::changeNodeName(int type) {
    ui->webview->page()->runJavaScript(
        QString("config.miami_node_name = %1").arg(type)
    );
    this->nodeName = type;
    this->updateUi(true);
}

void
MiamiApp::updatePath() {
    int start = ui->comboPathStart->currentData().toInt();
    int end = ui->comboPathEnd->currentData().toInt();
    QString code = QString("highlight_path(%1, %2)").arg(start).arg(end);
    ui->webview->page()->runJavaScript(code);
}

void
MiamiApp::updateUi(bool refreshSimulation) {

    // Update webview
    if (refreshSimulation) {
        ui->webview->page()->runJavaScript(QString("update(true)"));
    } else {
        ui->webview->page()->runJavaScript(QString("update(false)"));
    }
    // get node data from webview
    ui->webview->page()->runJavaScript("get_nodes()", [=](QVariant nodes) {
        this->modelMetabolites->removeRows(0, this->modelMetabolites->rowCount());
        ui->comboPathStart->clear();
        ui->comboPathEnd->clear();
        ui->comboPathStart->addItem("", -1);
        ui->comboPathEnd->addItem("", -1);


        this->statusNodes->setCount(nodes.toList().size());
        this->statusNodes->setVisible(true);

        for (auto metabolite : nodes.toList()) {
            QStringList data = metabolite.toStringList();
            // 2 = name; 5 = RT; 6 = RI

            QString name;
            switch (this->nodeName) {
                case nodeName::rtMin:
                    name = QString("RT %1min (%2)").arg(QString::number(data[5].toDouble()/1000/60, 'f', 2)).arg(data[2]);
                    this->modelMetabolites->setType(5);
                    break;
                case nodeName::rtSec:
                    name = QString("RT %1s (%2)").arg(QString::number(data[5].toDouble()/1000, 'f', 2)).arg(data[2]);
                    this->modelMetabolites->setType(5);
                    break;
                case nodeName::ri:
                    name = QString("RI %1 (%2)").arg(QString::number(data[6].toDouble(), 'f', 2)).arg(data[2]);
                    this->modelMetabolites->setType(6);
                    break;
                default:
                case nodeName::metaboliteName:
                    this->modelMetabolites->setType(2);
                    name = data[2];
                break;
            }

            int row = this->modelMetabolites->rowCount();
            this->modelMetabolites->insertRow(row);
            this->modelMetabolites->setData(this->modelMetabolites->index(row), name, Qt::DisplayRole);
            this->modelMetabolites->setData(this->modelMetabolites->index(row), data, Qt::UserRole);

            ui->comboPathStart->addItem(name, data[1]);
            ui->comboPathEnd->addItem(name, data[1]);
        }
        this->modelMetabolites->sort(0);
    });

    ui->webview->page()->runJavaScript("get_edges()", [=](QVariant edges) {
        this->statusEdges->setCount(edges.toList().size());
        this->statusEdges->setVisible(true);
    });

    // Get target data from webview
    ui->webview->page()->runJavaScript("get_targets()", [=](QVariant targets) {
        this->modelTargets->removeRows(0, this->modelTargets->rowCount());
        for (auto targ : targets.toList()) {
            int idx = targ.toList()[0].toInt();
            QString source = targ.toList()[1].toString();
            QString target = targ.toList()[2].toString();
            QString name = QString("%1 vs %2").arg(source).arg(target);
            int row = this->modelTargets->rowCount();
            this->modelTargets->insertRow(row);
            this->modelTargets->setData(this->modelTargets->index(row), name, Qt::DisplayRole);
            this->modelTargets->setData(this->modelTargets->index(row), idx, Qt::UserRole);

        }
        this->statusTargets->setCount(targets.toList().size());
        this->statusTargets->setVisible(true);
    });
    this->changeDirtiness(true);
}

void
MiamiApp::ping(const QString &s) const {
    qDebug() << "JS says: " <<s;
}

MiamiApp::~MiamiApp()
{
    this->console->close();
    delete this->ui;
    delete this->console;
    if (this->miami != nullptr) {
        delete this->miami;
    }
}
