// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef MIAMIAPP_H
#define MIAMIAPP_H

#include <QMainWindow>
#include <QtWidgets>
#include <QtConcurrent>

#include <miami/miami.hpp>
#include <miami/config.hpp>

#include "dialogaddcondition.h"
#include "datalistmodel.h"
#include "javascriptconsole.h"
#include "statuswidget.h"

class QWebEngineView;

enum nodeName {
    metaboliteName = 0,
    ri = 1,
    rtMin = 2,
    rtSec = 3
};

namespace Ui {
class MiamiApp;
}

class MiamiApp : public QMainWindow
{
    Q_OBJECT

public:
    explicit MiamiApp(QWidget *parent = nullptr);
    ~MiamiApp();

public:
    Q_INVOKABLE void ping(const QString &s) const;

private:
    Ui::MiamiApp *ui;


    miami::Miami *miami;
    miami::Config *config;

    QSettings *settings;
    QString current_file;
    QFont *fa;
    JavascriptConsole *console;

    QString json;

    StatusWidget *statusViz;
    StatusWidget *statusConf;
    StatusWidget *statusTargets;
    StatusWidget *statusAnalysis;
    StatusWidget *statusNodes;
    StatusWidget *statusEdges;
    QWidget *statusRunning;

    QHBoxLayout *legend_layout;

    QStringListModel *modelLibraries;
    QStringListModel *modelPathways;
    DataListModel *modelConditions;
    QStringListModel *modelReferenceRemove;
    DataListModel *modelTargets;
    DataListModel *modelMetabolites;

    DialogAddCondition *dialogAddCondition;



    QFutureWatcher<void> *threadMiami;

    size_t config_hash;
    bool webReady;
    bool flagDirty;
    int flagViz;
    int flagConf;
    int flagAnalysis;

    int nodeName;

    void initUi();
    void resetUi();
    void resetLegend();
    void configToUi();
    void initExperiments();

    void changeNodeName(int type);
    void updateUi(bool refreshSimulation);
    void changeDirtiness();
    void changeDirtiness(bool state);

    QString getStatusIcon(int status);
    void checkVisualization();

    void addPathway(QString filename);
    void addLibrary(QString filename);

private slots:
    void newFile();
    void openFile();
    void saveFile();
    void saveFileAs();
    void exportLibrary();

    void miamiStart();
    void miamiFinished();

    void addCondition(QString name, QStringList labeled, QStringList unlabeled, int row);
    void delCondition();
    void editCondition();

    void selectLibraries();
    void delLibrary();

    void selectPathways();
    void delPathway();

    void toggleShowSingleNodes();
    void toggleShowTargets();
    void toggleEdgesOverNodes();
    void toggleShowArrows();
    void toggleShowRefereceNodes();
    void toggleRemoveEdges();
    void updatePath();

    void setFa(QPushButton *button, QString icon);


};

#endif // MIAMIAPP_H
