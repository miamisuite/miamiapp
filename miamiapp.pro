#-------------------------------------------------
#
# Project created by QtCreator 2018-11-09T14:46:43
#
#-------------------------------------------------

QT += core gui

greaterThan(QT_MAJOR_VERSION, 5): QT += widgets printsupport

TARGET = miamiapp
TEMPLATE = app

QT += webenginewidgets concurrent
#QT += webkitwidgets
QT += webchannel

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17

SOURCES += \
        main.cpp \
        miamiapp.cpp \
        miamiwebenginepage.cpp \
    dialogaddcondition.cpp \
    datalistmodel.cpp \
    javascriptconsole.cpp \
    texthistoryedit.cpp \
    dialogimport.cpp \
    dialogricalibrator.cpp \
    qcustomplot.cpp \
    rtitemdelegate.cpp \
    statuswidget.cpp

HEADERS += \
        miamiapp.h \
        miamiwebenginepage.h \
    dialogaddcondition.h \
    datalistmodel.h \
    javascriptconsole.h \
    texthistoryedit.h \
    dialogimport.h \
    dialogricalibrator.h \
    qcustomplot.h \
    rtitemdelegate.h \
    statuswidget.h

FORMS += \
        miamiapp.ui \
    dialogaddcondition.ui \
    javascriptconsole.ui \
    dialogimport.ui \
    dialogricalibrator.ui

INCLUDEPATH += /usr/local/include /usr/local/include/miami /usr/local/include/gcms nlohmann

LIBS += -L"/usr/local/lib" -lmiami -lgcmslib -llabid -lz -lm -lgsl -lsqlite3 -lcblas -lnetcdf #-lboost_filesystem -lboost_regex -lboost_system

VERSION_MAJOR = 1
VERSION_MINOR = 0
VERSION_PATCH = 1

VERSION = $${VERSION_MAJOR}.$${VERSION_MINOR}.$${VERSION_PATCH}

DEFINES += MIAMI_VERSION_MAJOR=$$VERSION_MAJOR \
           MIAMI_VERSION_MINOR=$$VERSION_MINOR \
           MIAMI_VERSION_PATCH=$$VERSION_PATCH \
           MIAMI_VERSION=\\\"$$VERSION\\\"


# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    miamiapp.qrc

binfile.files += miamiapp
binfile.path = /usr/bin/
shortcutfiles.files += miamiapp.desktop
shortcutfiles.path = /usr/share/applications/
iconfiles.files += miamiapp.png
iconfiles.path = /usr/share/icons/

INSTALLS += binfile
INSTALLS += shortcutfiles
INSTALLS += iconfiles
