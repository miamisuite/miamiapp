# MIAMIapp fonts folder

MIAMIapp depends on [Fontawesome 5 free](https://fontawesome.com/v5/download). Download the Desktop version of Fontawesome and copy the following file to this folder:

-   `Font Awesome 5 Free-Solid-900.otf` (from the otfs folder)

The latest version tested is version 5.15.4.
