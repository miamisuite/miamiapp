// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "javascriptconsole.h"
#include "ui_javascriptconsole.h"

#include "miamiwebenginepage.h"

#include <QtWidgets>


JavascriptConsole::JavascriptConsole(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::JavascriptConsole)
{
    ui->setupUi(this);
    ui->editInput->setFocus();

    if (QFontDatabase::addApplicationFont(":/fonts/fonts/Font Awesome 5 Free-Solid-900.otf") >= 0) {
        QFont fa("Font Awesome 5 Free", 12, 0, false);
        fa.setStyleName("Solid");

        ui->buttonClear->setFont(fa);
        ui->buttonClear->setText("\uf2ed");
    }

    connect(ui->buttonClear, &QPushButton::clicked, [=]() {
       ui->editOutput->clear();
    });

    connect(ui->editInput, &QLineEdit::returnPressed, [=]() {
        QString code = ui->editInput->text();
        this->page->runJavaScript(QString("runCode(%1);").arg(code));
        this->history << code;
        ui->editInput->clear();
    });
}

void
JavascriptConsole::setPage(MiamiWebEnginePage *page)
{
    this->page = page;
    this->page->registerConsole(this);
}

void
JavascriptConsole::log(const QString &msg, int line, const QString &file) {
    ui->editOutput->appendHtml(QString("%3<span style=\"font-size:8px; color: #666666;\">(%1:%2)</span>").arg(file).arg(line).arg(msg));
}


JavascriptConsole::~JavascriptConsole()
{
    delete ui;
}
