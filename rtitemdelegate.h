// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef RTITEMDELEGATE_H
#define RTITEMDELEGATE_H

#include <QMap>
#include <QComboBox>
#include <QStyledItemDelegate>

class RtItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    explicit RtItemDelegate(QMap<QString, QList<double> > *mapping, QObject *parent = nullptr);
    ~RtItemDelegate() override;

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
    void setComboBoxData(QStringList data);
private:
    QComboBox *cb;
    QMap<QString, QList<double>> *mapping;
};

#endif // RTITEMDELEGATE_H
