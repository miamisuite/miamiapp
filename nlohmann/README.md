# JSON for Modern C++

MIAMI depends on the [JSON library](https://github.com/nlohmann/json) by Niels Lohmann

Please copy the [single include `json.hpp`](https://github.com/nlohmann/json/releases/tag/v3.10.5) file to this folder.
