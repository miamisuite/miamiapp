// MIAMIapp - Full Qt application for Mode of Action analysis
//    Copyright (C) 2019 Christian-Alexander Dudek

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "dialogricalibrator.h"
#include "ui_dialogricalibrator.h"

#include "qcustomplot.h"

#include <ricalculator.h>
#include <librarysearch.h>
#include <gcmsdiskscan.h>

#include <QtWidgets>
#include <QtConcurrent>

DialogRiCalibrator::DialogRiCalibrator(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogRiCalibrator)
{
    settings = new QSettings("TU Braunschweig", "MiamiViz");

    ui->setupUi(this);

    ui->buttonToMix->setEnabled(false);
    ui->buttonOpenMix->setEnabled(false);
    ui->buttonCalibrate->setEnabled(false);
    ui->buttonDelFile->setEnabled(false);

    ui->barStatus->setVisible(false);
    ui->labelStatus->setVisible(false);

    ui->buttonOpenLib->setFocus();

    ui->tableRi->setColumnCount(3);
    ui->tableRi->setHorizontalHeaderLabels({"Compound", "RT [min]", "RI"});

    ui->listFiles->setModel(new QStringListModel());

    if (QFontDatabase::addApplicationFont(":/fonts/fonts/Font Awesome 5 Free-Solid-900.otf") >= 0) {
        QFont fa("Font Awesome 5 Free", 12, 0, false);
        fa.setStyleName("Solid");

        ui->buttonToMix->setFont(fa);
        ui->buttonToMix->setText("\uf362");

        ui->buttonAddFile->setFont(fa);
        ui->buttonAddFile->setText("\uf067");

        ui->buttonDelFile->setFont(fa);
        ui->buttonDelFile->setText("\uf068");

        ui->buttonOpenLib->setFont(fa);
        ui->buttonOpenLib->setText("\uf07c");

        ui->buttonOpenMix->setFont(fa);
        ui->buttonOpenMix->setText("\uf07c");

        ui->buttonOpenMix->resize(ui->buttonOpenMix->sizeHint().width(), ui->buttonOpenMix->sizeHint().height());
    }


    this->thread = new QFutureWatcher<void>();

    connect(this->thread, &QFutureWatcher<void>::finished, [=]() {
        ui->buttonCancel->setEnabled(true);
        ui->labelStatus->setText(QString("%1/%2 files successfully calibrated!").arg(this->cntOk).arg(this->cntAll));
        ui->barStatus->setVisible(false);
    });

    ui->editLib->setText(settings->value("last_lib", "").toString());

    if (ui->editLib->text() != "") {
        this->openLibrary(ui->editLib->text());
    }

    for (auto item : settings->value("last_mix_list", QStringList()).toStringList()) {
        auto lst = ui->listLib->findItems(item, Qt::MatchExactly);
        if (lst.size() > 0) {
            ui->listMix->addItem(item);
            delete lst[0];
        }
    }

    if (ui->editLib->text() != "" && ui->listMix->count() > 0) {
        ui->buttonOpenMix->setEnabled(true);
        ui->buttonOpenMix->setFocus();
    }

    this->plotSpectrum = new QCustomPlot();
    connect(this->plotSpectrum, &QCustomPlot::mousePress, [=](QMouseEvent* e) {
        if (e->button() == Qt::RightButton) {
            this->plotSpectrum->rescaleAxes();
            this->plotSpectrum->replot();
        }
    });

    connect(ui->buttonOpenLib, &QPushButton::clicked, [=]() {
        QVariant last = settings->value("last_lib", QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));
        QString file = QFileDialog::getOpenFileName ( this, "Open compound library", last.toString(), "MD library (*.lbr)");

        ui->listLib->clear();
        ui->listMix->clear();

        if (file != "") {
            this->openLibrary(file);
            settings->setValue("last_lib", file);
        }
    });

    connect(ui->tableRi, &QTableWidget::itemChanged, [=]() {
        if(ui->tabWidget->count() == 1) {
            ui->tabWidget->addTab(plotSpectrum, "Spectrum");
        }
        replotSpectrum();
    });
    connect(ui->tableRi, &QTableWidget::cellClicked, [=]() {
        if(ui->tabWidget->count() == 1) {
            ui->tabWidget->addTab(plotSpectrum, "Spectrum");
        }
        replotSpectrum();
    });


    connect(ui->listFiles, &QListWidget::clicked, [=](){
        ui->buttonDelFile->setEnabled(true);
    });

    connect(ui->buttonDelFile, &QPushButton::clicked, [=]() {
        QString file = ui->listFiles->currentIndex().data().toString();
        auto itr = std::find(this->calibrate_files.begin(), this->calibrate_files.end(), file);
        this->calibrate_files.erase(itr);
        ui->listFiles->model()->removeRows(ui->listFiles->currentIndex().row(), 1);

        if (ui->listFiles->model()->rowCount() == 0) {
            ui->buttonDelFile->setEnabled(false);
            ui->buttonCalibrate->setEnabled(false);
        }
    });

    connect(ui->buttonAddFile, &QPushButton::clicked, [=]() {
        QVariant last = settings->value("last_calibrated", QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));

        QFileDialog dialog(this, tr("Open chromatograms to calibrate"));
        dialog.setFileMode(QFileDialog::ExistingFiles);
        dialog.setDirectory(last.toString());
        dialog.setNameFilter("MD compound files (*.cmp)");
        if (dialog.exec()) {
            QStringList files = dialog.selectedFiles();
            for (auto file : files) {
                if (!calibrate_files.contains(file)) {
                    this->calibrate_files.push_back(file);
                    int row = ui->listFiles->model()->rowCount();
                    ui->listFiles->model()->insertRow(row);
                    ui->listFiles->model()->setData(ui->listFiles->model()->index(row, 0), file, Qt::EditRole);
                }
            }
            settings->setValue("last_calibrated", QFileInfo(files[0]).path());
        }

        ui->buttonCalibrate->setEnabled(true);

    });

    connect(ui->buttonToMix, &QPushButton::clicked, [=]() {
        // Focus on lib list
        if(ui->listLib->currentRow() >= 0) {
            ui->listMix->addItem(ui->listLib->currentItem()->text());
            delete ui->listLib->takeItem(ui->listLib->currentRow());
            ui->buttonOpenMix->setEnabled(ui->listMix->count() > 0);
        // Focus on mix list
        } else if(ui->listMix->currentRow() >= 0) {
            ui->listLib->addItem(ui->listMix->currentItem()->text());
            delete ui->listMix->takeItem(ui->listMix->currentRow());
            ui->listLib->sortItems();
            ui->buttonOpenMix->setEnabled(ui->listMix->count() > 0);
        }
    });

    connect(ui->listFiles, &QListWidget::clicked, [=](){
        ui->listMix->setCurrentRow(-1);
    });
    connect(ui->listMix, &QListWidget::clicked, [=](){
        ui->listLib->setCurrentRow(-1);
    });

    connect(ui->buttonOpenMix, &QPushButton::clicked, [=]() {
        QVariant last = settings->value("last_mix_file", QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));
        QString cmpFile = QFileDialog::getOpenFileName ( this, "Open alkane mix chromatogram", last.toString(), "MD compound file (*.cmp)");
        if (cmpFile != "") {
            ui->editMix->setText(cmpFile);
            QString baseFile = cmpFile;
            baseFile.remove(".cmp");

            // Load data
            compounds = gcms::Compound<int, float>::fromDisk(cmpFile.toStdString().c_str());
            chromatogram = new gcms::GCMSDiskScan<int,float>(baseFile.toStdString().c_str());

            gcms::GCMSSettings::LS_USE_SPEC = true;
            gcms::GCMSSettings::LS_USE_RI = false;
            gcms::GCMSSettings::LS_RI_DIFF = 20;
            gcms::GCMSSettings::LS_CUTOFF = 0.7;

            // Find compounds in mix file

            // Mapping Compound name = {score, rt, ri, int, cmp_idx}
            mappingAll = {};
            mappingSelected = {};

            int c = 0;
            for (auto comp : compounds) {
                std::vector<gcms::LibraryHit<int, float>> hits = this->lib->getLibraryHits(*comp);
                for (auto hit : hits) {
                    QString hitName = QString::fromStdString(hit.getLibraryCompound()->getName());
                    int best = -1;
                    if (mappingSelected.contains(hitName)) {
                        best = mappingSelected[hitName];
                    }
                    // Good score and contained in mix list
                    if (hit.getOverallScore() > 0.7 &&  ui->listMix->findItems(hitName, Qt::MatchExactly).size() > 0) {
                        int idx = mappingAll[hitName].size();
                        mappingAll[hitName].push_back(
                            {hit.getOverallScore(), comp->getRetentionTime(), hit.getLibraryCompound()->getRetentionIndex(), comp->getTotalSignal(), static_cast<double>(c)}
                        );
                        if (best == -1) {
                            mappingSelected[hitName] = idx;
                        } else if (hit.getOverallScore() > mappingAll[hitName][best][0]) {
                            mappingSelected[hitName] = idx;
                        }
                    }
                }
                c++;
            }

//            ui->tableRi->clear();
            ui->tableRi->setRowCount(0);

            // Create calibration table
            for (auto key : mappingSelected.keys()) {
                QList<double> cur = mappingAll[key][mappingSelected[key]];

                int row = ui->tableRi->rowCount();
                ui->tableRi->insertRow(row);
                ui->tableRi->setItem(row, 0, new QTableWidgetItem(key));
                ui->tableRi->item(row, 0)->setFlags(ui->tableRi->item(row, 0)->flags() ^ Qt::ItemIsEditable);

                QComboBox* cb = new QComboBox();
                cb->setProperty("row", row);
                for (auto values : mappingAll[key]) {
                    cb->addItem(QString::number(values[1]/1000/60, 'd', 2));
                }
                cb->setCurrentIndex(mappingSelected[key]);
                connect(cb, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int i) {
                    ui->tableRi->setCurrentCell(row, 0);
                    mappingSelected[key] = i;
                    this->replotTic();
                    this->replotSpectrum();
                });

                ui->tableRi->setCellWidget(row, 1, cb);

                ui->tableRi->setItem(row, 2, new QTableWidgetItem(QString::number(cur[2], 'i', 0)));
                ui->tableRi->item(row, 2)->setFlags(ui->tableRi->item(row, 2)->flags() ^ Qt::ItemIsEditable);
            }

            ui->tableRi->resizeColumnsToContents();

            this->replotTic();

            ui->buttonAddFile->setFocus();

            // Save mix to settings
            QStringList mix;
            for (int i=0; i < ui->listMix->count(); i++) {
                mix << ui->listMix->item(i)->text();
            }

            settings->setValue("last_mix_list", mix);
            settings->setValue("last_mix_file", cmpFile);
        }
    });

    connect(ui->buttonCancel, &QPushButton::clicked, [=]() {
       this->close();
       delete this;
    });

    connect(ui->buttonCalibrate, &QPushButton::clicked, [=]() {

        ui->barStatus->setVisible(true);
        ui->labelStatus->setText("Calibrating...");
        ui->labelStatus->setVisible(true);
        ui->buttonCalibrate->setEnabled(false);
        ui->buttonCancel->setEnabled(false);

        if (this->thread->isRunning()) {
            thread->cancel();
            thread->waitForFinished();
        }

        this->thread->setFuture(QtConcurrent::run([=](){
            this->calibrateFiles();
        }));

    });
}

void
DialogRiCalibrator::replotSpectrum() {

    int row = ui->tableRi->currentRow();

    if (row == -1) {
        return;
    }
    this->plotSpectrum->clearItems();
    this->plotSpectrum->clearGraphs();

    QString name = ui->tableRi->item(row, 0)->text();
    int idx = this->mappingSelected[name];

    QList<double> lst = this->mappingAll[name][idx];

    size_t cmp_idx = static_cast<size_t>(lst[4]);
    auto cmp = compounds[cmp_idx];

    auto m = cmp->getMasses();
    auto v = cmp->getValues();


    ui->tabWidget->setTabText(1, QString("RT %2").arg(lst[1]/60/1000));

    QVector<double> masses = {};
    QVector<double> values = {};
    for (size_t i=0;i < m.size(); i++) {

        values << static_cast<double>(v[i]);
        masses << static_cast<double>(m[i]);
        if (i > 0 && i < m.size()-1 && v[i-1] < v[i] && v[i+1] < v[i]) {
            QCPItemText *label = new QCPItemText(this->plotSpectrum);
            label->setPositionAlignment(Qt::AlignBottom | Qt::AlignHCenter);
            label->position->setCoords(static_cast<double>(m[i]), v[i]);
            label->setText(QString::number(static_cast<double>(m[i]), 'i', 0));
            label->setClipToAxisRect(false);
        }
    }

    this->plotSpectrum->addGraph();
    this->plotSpectrum->graph()->setData(masses, values);

    this->plotSpectrum->graph()->setLineStyle(QCPGraph::lsImpulse);
    this->plotSpectrum->setInteraction(QCP::iRangeZoom, true);
    this->plotSpectrum->setSelectionRectMode(QCP::srmZoom);
    this->plotSpectrum->rescaleAxes();
    this->plotSpectrum->xAxis->setLabel("m/z");
    this->plotSpectrum->yAxis->setLabel("Intensity");

    this->plotSpectrum->replot();
}

void
DialogRiCalibrator::replotTic() {
    // Plot chromatogram
    ui->plotChromatogram->clearItems();
    ui->plotChromatogram->clearGraphs();
    QVector<double> times;
    for(auto t : chromatogram->getTIC()->getTimes()) {
        times << t/1000/60;
    }
    QVector<double> values = QVector<double>::fromStdVector(chromatogram->getTIC()->getValues());
    ui->plotChromatogram->addGraph();
    ui->plotChromatogram->graph()->setData(times, values);

    QVector<double> rts;
    QVector<double> ints;

    // Mapping Compound name = {score, rt, ri, int, cmp_idx}
    for (auto key : mappingSelected.keys()) {
        auto selection = mappingAll[key][mappingSelected[key]];
        double rt = selection[1]/1000/60;
        rts.push_back(rt);
        ints.push_back(selection[3]);

        QCPItemText *label = new QCPItemText(ui->plotChromatogram);
        label->setPositionAlignment(Qt::AlignBottom | Qt::AlignHCenter);
        label->position->setCoords(rt, selection[3]);
        label->setText(QString::number(selection[2], 'i', 0));
        label->setClipToAxisRect(false);
    }

    // Plot marker
    ui->plotChromatogram->addGraph();
    ui->plotChromatogram->graph()->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssTriangleInverted, Qt::red, Qt::red, 5));
    ui->plotChromatogram->graph()->setLineStyle(QCPGraph::lsNone);
    ui->plotChromatogram->graph()->setData(rts, ints);

    ui->plotChromatogram->setInteraction(QCP::iRangeZoom, true);
    ui->plotChromatogram->setInteraction(QCP::iRangeDrag, true);
    ui->plotChromatogram->rescaleAxes();
    ui->plotChromatogram->xAxis->setLabel("Time [min]");
    ui->plotChromatogram->yAxis->setLabel("Intensity");
    ui->plotChromatogram->replot();

}

void
DialogRiCalibrator::calibrateFiles() {
    // Load calibration table
    gcms::RICalculator ric = this->chromatogram->getRICalculator();
    for (auto key : this->mappingSelected.keys()) {
        int idx = this->mappingSelected[key];
        double rt = this->mappingAll[key][idx][1];
        double ri = this->mappingAll[key][idx][2];
        ric.addCalibrationPair(rt, ri);
    }

//    gcms::GCMSSettings::GCMSSettingsObject settings(gcms::GCMSSettings::getGCMSSettingsObject());
    this->cntOk = 0;
    this->cntAll = 0;
    for (auto file : this->calibrate_files) {
        this->cntAll++;
        auto compounds = gcms::Compound<int, float>::fromDisk(file.toStdString().c_str());
        bool res = ric.calculateRI(compounds);
        if (res) {
            this->cntOk ++;
        }
        gcms::Compound<int, float>::toDisk(compounds, file.toStdString().c_str());
    }
}

void
DialogRiCalibrator::openLibrary(const QString &file) {

    ui->editLib->setText(file);
    this->lib = gcms::LibrarySearch<int, float>::fromDisk(file.toStdString().c_str());

    for (auto comp : this->lib->getLibraryCompounds()) {
        ui->listLib->addItem(QString::fromStdString(comp->getName()));
    }
    ui->listLib->sortItems();
    ui->buttonToMix->setEnabled(true);
    ui->buttonToMix->setFocus();
}

DialogRiCalibrator::~DialogRiCalibrator()
{
    delete this->lib;
    delete this->chromatogram;
    delete ui;
}
